package unq.moovies.Genero;

import java.util.ArrayList;
import java.util.List;

import unq.moovies.project.Notificacion;
import unq.moovies.project.Usuario;

//Responsabilidad:Describe un genero al que puede pertenecer un o mas pelicula y permite que los usarios
//				  del sistema se suscriban a dicho genero.
public abstract class Genero {

	//Estructura
	private List<Usuario> 	coleccionDeUsuario;
	private String 			nombreGenero;
	
	public Genero(){
		this.coleccionDeUsuario		= new ArrayList<Usuario>();
		this.setNombreGenero("");
	}
		
	//Proposito:Retorna el nombre Del Genero
	//Precondicion:-
	public String getNombreGenero() {
		return nombreGenero;
	}

	//Proposito:setea el nombre del genero<unNombreGenero>
	//Precondicion:
	public void setNombreGenero(String unNombreGenero) {
		this.nombreGenero = unNombreGenero;
	}
	
	
	//Proposito:agrega un usuario a la coleccion de usuario<unUsuario>
	//Precondicion:-
	public void subscribirse(Usuario unUsuario) {
		
		this.coleccionDeUsuario.add(unUsuario);
	}
	
	//Proposito:borra a un usuario<unUsuario> de la coleccion de usuario.
	//Precondicion:--
	public void desubcribirse(Usuario unUsuario) {
		this.coleccionDeUsuario.remove(unUsuario);
		
	}
	//Proposito:retorna la coleccion de usuarios
	//Precondicion:-
	public List<Usuario> getColeccionDeUsuario() {
		return coleccionDeUsuario;
	}
	//Proposito:sete la coleccion de usuarios <unaColeccionDeUsuarios>
	//Precondicion:-
	public void setColeccionDeUsuario(List<Usuario> unaColeccionDeUsuarios) {
		this.coleccionDeUsuario = unaColeccionDeUsuarios;
	}
	
	//Protocolo
	public abstract void notificar(Notificacion unaNotifiacion); 
	public abstract List<Genero> compactar();
}
