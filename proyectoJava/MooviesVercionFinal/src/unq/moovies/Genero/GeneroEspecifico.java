package unq.moovies.Genero;

import java.util.ArrayList;
import java.util.List;

import unq.moovies.project.Notificacion;
import unq.moovies.project.Usuario;

//Responsabilidad: Representa un Genero el cual es mas especifico
//				   y tambien guarda otros gneeros mas genericos
//				   con los que guarda relacion. 
//				   permite a un usuario suscribirse a ese genero,
//				   y que todos los generos con los que
//				   guarda relacion se enteres cuando se agregar
//				   una pelicula
public class GeneroEspecifico extends Genero  {

	private List<Genero> coleccionDeGenero;
	
	public GeneroEspecifico(String unNombreGenero){
		super();
		this.setColeccionDeGenero(new ArrayList<Genero>());
		this.setNombreGenero(unNombreGenero);
	}
	
	public List<Genero> getColeccionDeGenero() {
		return coleccionDeGenero;
	}

	public void setColeccionDeGenero(List<Genero> coleccionDeGenero) {
		this.coleccionDeGenero = coleccionDeGenero;
	}

	//Protocolo
	public void agregar(Genero unGenero) {
		this.coleccionDeGenero.add(unGenero);
		
	}

	public void eliminar(Genero unGenero) {
		this.coleccionDeGenero.remove(unGenero);
	}


	@Override
	public void notificar(Notificacion unaNotificacion) {
		for(Usuario unUsuario: this.getColeccionDeUsuario()){
			unUsuario.agregarNotificacion(unaNotificacion);
		}
		
		for(Genero unGenero: this.coleccionDeGenero){
			
			unGenero.notificar(unaNotificacion);
		}	
	}

	@Override
	public List<Genero> compactar() {
		List<Genero> respuesta=new ArrayList<Genero>();
		respuesta.add(this);
		for(Genero unGenero : this.getColeccionDeGenero()){
			respuesta.addAll(unGenero.compactar());
		}
		
		return respuesta;
	}


}
