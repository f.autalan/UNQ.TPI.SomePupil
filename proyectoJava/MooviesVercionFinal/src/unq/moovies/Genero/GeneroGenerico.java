package unq.moovies.Genero;

import java.util.ArrayList;
import java.util.List;

import unq.moovies.project.Notificacion;

import unq.moovies.project.Usuario;

//Responsabilidad: Representa un genero que es parte de una cantidad 
// 				   indeterminada de genero mas especificos.
//				   Permite a un usuario suscribirse a ese genero
//				   y ser notificados cuando sale una pelicula.
public class GeneroGenerico extends Genero {
	
	
	public GeneroGenerico(String unNombreGenero){
		super();
		this.setNombreGenero(unNombreGenero);
	}
	
	//Protocolo
	@Override
	public void notificar(Notificacion unaNotificacion) {
		for(Usuario unUsuario: getColeccionDeUsuario()){
			unUsuario.agregarNotificacion(unaNotificacion);
		}
	}

	@Override
	public List<Genero> compactar() {
		List<Genero> respuesta=new ArrayList<Genero>();
		respuesta.add(this);
		
		return respuesta;
	}


	


}
