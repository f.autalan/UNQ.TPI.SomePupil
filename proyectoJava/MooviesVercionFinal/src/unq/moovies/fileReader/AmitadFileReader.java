package unq.moovies.fileReader;

import unq.moovies.project.Amistad;


//Responsabilidad: lee los datos de un archivo de amistad
//				   y devuelve una lista con todos los elemento leidos
public class AmitadFileReader extends CSVFileReader<Amistad> {
	
	public AmitadFileReader(String filePath) {
		super(filePath);
		
	}

	/**
     * @param filePath the absolute path of the file to be read.
     */
	 //Protocolo
	@Override
	protected Amistad  parseLine(String[] line) {

       Amistad amistadADevolver =new Amistad(line[0],line[1]);
    	
        return amistadADevolver;
    }

}
