package unq.moovies.fileReader;

import java.util.ArrayList;


import unq.moovies.project.Pelicula;
import unq.moovies.project.PublicInfo;

/**
 * Created by gabriel on 12/04/17.
 */

//Responsabilidad: lee los datos de un archivo de Pelicula
//				   y devuelve una lista con todos los elemento leidos
public class PeliculaFileReader extends CSVFileReader<Pelicula> {

    /**
     * @param filePath the absolute path of the file to be read.
     */
    public PeliculaFileReader(String filePath) {
        super(filePath);
    }
    
    //Protocolo
    @Override
    protected Pelicula parseLine(String[] line) {
        
    	
    	ArrayList<String> coleccionDeGenerosDePerfil = new ArrayList<String>();
    	ArrayList<String> coleccionDeGeneros= this.get_generos();
    	Integer i=4;
    	
    
    	for(String unGenero: coleccionDeGeneros){
    		if(line[i].equals(" 1 ")){
    			coleccionDeGenerosDePerfil.add(unGenero);
    		}
    		i++;
    	}
    	
       PublicInfo publicInfoDePelicula= new PublicInfo(line[1], line[2], line[3], coleccionDeGenerosDePerfil );
       Pelicula peliculaADevolver = new Pelicula(publicInfoDePelicula) ;
    	
        return peliculaADevolver;
    }
   
	

	public ArrayList<String> get_generos(){
    	ArrayList<String> generos = new ArrayList<String>();
    	generos.add("Unknown");//0
    	generos.add("Action");//1
    	generos.add("Adventure");//2
    	generos.add("Animation");//3
    	generos.add("Children's");//4
    	generos.add("Comedy");//5
    	generos.add("Crime");//6
    	generos.add("Documentary");//7
    	generos.add("Drama");//8
    	generos.add("Fantasy");//9
    	generos.add("Film-Noir");//10
    	generos.add("Horror");//11
    	generos.add("Musical");//12
    	generos.add("Mystery");//13
    	generos.add("Romance");//14
    	generos.add("Sci-Fi");//15
    	generos.add("Thriller");//16
    	generos.add("War");//17
    	generos.add("Western");//18
    	
    	
    	
    	return generos;
    }
}
