package unq.moovies.fileReader;

import unq.moovies.sistemaDeCalificacion.Calificacion;


//Responsabilidad: lee los datos de un archivo de Rating
//				   y devuelve una lista con todos los elemento leidos
public class RatingFileReader extends CSVFileReader<Calificacion> {
	
	    public RatingFileReader(String filePath) {
	        super(filePath);
	    }
	    
	    //Protocolo
	    @Override
	    protected Calificacion parseLine(String[] line) {
	
		    //Nota: Lo que llega del filereader es un string, pero el sistema trabaja con integers para las calificaciones (Es necesario hacer comparaciones y sacar promedios)
		    //Lo ideal seria utilizar el mensaje Integer.parseToInt para pasar el string a int, pero No se puede utilizar el mensaje por que el string que viene con el filereader tiene un espacio antes del numero.
	    	//Uso trim para sacarle el espacio y despues pasarlo a Integer.
	    	
	    	String ratingString = line[3];
	    	String stringAParsear= ratingString.trim();
	    	Integer unRating = Integer.parseInt(stringAParsear);
	    	
	    	//Nota: Al igual que con el rating, le saco los espacios vacios al resto de las lineas:
	    	
	    	String unUserId = line[0].trim();
	    	String unPeliculaId= line[2].trim();
	    	String unTimeStamp= line[1].trim();
	    	
	    	//fileReader:   user id | ti.trim().trim()mestamp | movie id | rating
	    	//               line[0]    line[1]    line[2]   line[3]
	    	
	    	
	    	//Constructor: Calificacion(String idDeUsuario, String idDePelicula,String unTimeStamp, Integer unRating)
	    	
	    	Calificacion calificacionARetornar = new Calificacion(unUserId,unPeliculaId,unTimeStamp,unRating);
	    	return calificacionARetornar;
	    }

}
