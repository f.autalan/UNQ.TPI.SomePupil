package unq.moovies.fileReader;

import unq.moovies.project.ProfileUsuario;
import unq.moovies.project.Usuario;

/**
 * Created by gabriel on 12/04/17.
 */

//Responsabilidad: lee los datos de un archivo de Usuario
//				   y devuelve una lista con todos los elemento leidos
public class UsuarioFileReader extends CSVFileReader<Usuario> {
	/**
     * @param filePath the absolute path of the file to be read.
     */
    public UsuarioFileReader(String filePath) {
        super(filePath);
    }
    
    //Protocolo
    @Override
    protected Usuario parseLine(String[] line) {

    	
       ProfileUsuario profileUsuario= new ProfileUsuario( line[5], line[6],line[1], line[4], line[3]);
       Usuario usuarioADevolver = new Usuario(line[5],"vacio",profileUsuario,line[0]);
    	
        return usuarioADevolver;
    }


}
