package unq.moovies.project;

//Responsabilidad: Representa la relacion de amistad entablada entre dos usuarios por sus id.
public class Amistad {
	private String idEmisor;
	private String idReceptor;
	
	
	public Amistad(String unId, String otroId){
		this.setIdEmisor(unId);
		this.setIdReceptor(otroId);
	}


	public String getIdEmisor() {
		return idEmisor;
	}

	public String getIdReceptor() {
		return idReceptor;
	}

	public void setIdEmisor(String idEmisor) {
		this.idEmisor = idEmisor;
	}

	public void setIdReceptor(String idRemisor) {
		this.idReceptor = idRemisor;
	}

}
