package unq.moovies.project;

//Responsabilidad:Son los objetos que pueden ser identificados por nombre o por id en el sistema.

public interface Identificable {

	public String getNombre();
	public String getId();
}
