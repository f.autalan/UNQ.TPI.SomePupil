package unq.moovies.project;

import java.util.List;

import unq.moovies.fileReader.AmitadFileReader;
import unq.moovies.fileReader.PeliculaFileReader;
import unq.moovies.fileReader.RatingFileReader;
import unq.moovies.fileReader.UsuarioFileReader;
import unq.moovies.sistemaDeCalificacion.Calificacion;

//Responsabilidad:Proveer a moovis de los diferentes datos externos
//					los cuales esta interesado en integrar al sistema.
//					para esto guarda las diferentes rutas de las cuales 
//					moovis desea importar datos, y utiliza los diferentes
//					tipos de file reader.

public class MoovieLens {
	
	//Estructura
	
	//El sistema guarda la ultima ruta utilizada; Esto es por si moovies necesita recuperar informacion sin necesidad de estar pasando una ruta constantemente. 
	//Que la responsabilidad de recordar una ruta en particular de donde importar informacion sea del Sistema de importacion, no de moovies.
	//Sabemos que por ahora solo se necesita informacion de una sola base de datos (la de IDBM), de necesitar cambiarse, posiblemente habria que reconsiderar la estructura.
	
	private String rutaDePeliculas;
	private String rutaDeUsuarios;
	private String rutaDeAmistades;
	private String rutaDeCalificaciones;
	
	//Constructores
	
	public MoovieLens(){
		super();	
	}

	//Protocolo
	
	public void setRutaPeliculas(String unaRuta){
		this.rutaDePeliculas=unaRuta;
	}
	public void setRutaUsuarios(String unaRuta){
		this.rutaDeUsuarios=unaRuta;
	}
	public void setRutaAmistades(String unaRuta){
		this.rutaDeAmistades=unaRuta;
	}
	public void setRutaCalificaciones(String unaRuta){
		this.rutaDeCalificaciones=unaRuta;
	}
	
	public List<Pelicula> importarPeliculas(){
		PeliculaFileReader peliculasReader= new PeliculaFileReader(this.rutaDePeliculas);
		List<Pelicula> coleccionDePelicula = peliculasReader.readFile();
		return coleccionDePelicula;
		
	}
	
	public List<Usuario> importarUsuarios(){
		UsuarioFileReader usuariosReader= new UsuarioFileReader(this.rutaDeUsuarios);
		List<Usuario> coleccionDeUsuario = usuariosReader.readFile();
		return coleccionDeUsuario;
	}
	public List<Amistad> importarAmistad(){
		AmitadFileReader amistadReader=new AmitadFileReader(this.rutaDeAmistades);
		List<Amistad> colleccionDeAmistades= amistadReader.readFile();
		return colleccionDeAmistades;
	}
	public List<Calificacion> importarCalificaciones(){
		RatingFileReader raitingReader=new RatingFileReader(this.rutaDeCalificaciones);
		List<Calificacion> colleccionDeRatings= raitingReader.readFile();
		return colleccionDeRatings;
	}
}
