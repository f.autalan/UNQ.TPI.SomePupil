package unq.moovies.project;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import unq.moovies.sistemaDeAdministradores.AdministradorDePeliculas;
import unq.moovies.sistemaDeAdministradores.AdministradorDeUsuarios;
import unq.moovies.sistemaDeCalificacion.Calificacion;
import unq.moovies.sistemaDeRecomendacion.MetodoDeRecomendacion;
import unq.moovies.sistemaDeRecomendacion.Recomendador;
import unq.moovies.sistemaInicioDeSesion.Identificacion;
import unq.moovies.sistemaInicioDeSesion.SistemaDeInicioDeSesion;

//Responsabilidad: sirve como tronco del sistema y es quien conoce y interactura con los diferentes
//					objetos claves para llevar a cabo las tareas que se espera que el sistema cumpla

public class Moovies {

	//Estructura
	
	private AdministradorDeUsuarios administradorDeUsuarios;
	private AdministradorDePeliculas administradorDePeliculas;
	private SistemaDeInicioDeSesion iniciarSesion;
	private MoovieLens sistemaDeImportacion;
	
	
	
	//Constructores
	
	public Moovies(){
		super();
		this.administradorDeUsuarios 	= new AdministradorDeUsuarios();
		this.administradorDePeliculas 	= new AdministradorDePeliculas();
		this.iniciarSesion				= new SistemaDeInicioDeSesion();
		this.sistemaDeImportacion 		= new MoovieLens();
		
	}	
	
	//Getters
	
	//Proposito: Devuelve una coleccion con las calificaciones de una pelicula.
	//Precondicion: La pelicula tiene que estar registrada en el sistema.
	public ArrayList<Calificacion> getCalificacionesDePelicula(Pelicula unaPelicula) {
		return this.administradorDePeliculas.getCalificacionesDePelicula(unaPelicula);
	}

	//Proposito: Devuelve la calificacion promedio de una pelicula.
	//Precondicion: La pelicula tiene que estar registrada en el sistema.
	public Integer getCalificacionPromedioDePelicula(Pelicula unaPelicula) {
		return this.administradorDePeliculas.getCalificacionPromedioDePelicula(unaPelicula);
	}

	//Proposito: Devuelve la coleccion de todas las calificaciones que dio un usuario.
	//Precondicion: El usuario tiene que estar registrado en el sistema.
	public ArrayList<Calificacion> getCalificacionesDeUsuario(Usuario unUsuario) {
		return this.administradorDeUsuarios.getCalificacionesDeUsuario(unUsuario);
	}

	//Proposito: Devuelve la cantidad promedio del rating que le dio a todas sus calificaciones un usuario en particular.
	//Precondicion: El usuario tiene que estar registrado en el sistema.
	//Si el usuario no dio calificaciones, devuelve 0.
	public Integer getCalificacionesPromedioDeUsuario(Usuario unUsuario) {
		return this.administradorDeUsuarios.getCalificacionesPromedioDeUsuario(unUsuario);
	}

	//Proposito: Devuelve la coleccion de las peliculas con mayor promedio en orden descendente
	// Si no hay peliculas en el sistema, el resultado va a estar vacio
	public ArrayList<Pelicula> get10PelisConMayorPromedioDescendente() {
		return this.administradorDePeliculas.get10UsuariosConMayorCalificacionesDescendente();
	}

	//Proposito: Devuelve la coleccion de usuarios con mayor cantidad de calificaciones en orden descendente
	// Si no hay usuarios en el sistema, el resultado va a estar vacio
	public ArrayList<Usuario> get10UsuariosConMayorCalificacionesDescendente() {
		return this.administradorDeUsuarios.get10UsuariosConMayorCalificacionesDescendente();
	}

	//proposito: dado un arraylist de ids, devuelve las peliculas que corresponden a esos ids
	public ArrayList<Pelicula> getPeliculasConId(List<String> idPeliculas) {
		return this.administradorDePeliculas.getPeliculasConId(idPeliculas);
	}
	
	
	//Setters
	
	//Protocolo
	
	
	// Proposito: Agrega un usuario al sistema.
	public void agregarUsuario(Usuario unUsuario) {
		this.administradorDeUsuarios.agregarElemento(unUsuario);
	}

	// Proposito: Contesta si tiene a un usuario Registrado en el sistema. Si esta, devuelve True.
	public boolean tieneRegistradoAUsuario(Usuario unUsuario) {
		return (this.administradorDeUsuarios.tieneRegistradoAElemento(unUsuario));
	}

	// Proposito: Devuelve una lista de posibles resultados de una busqueda por nombre. Si no encuentra nada, el resultado estara vacio.
	public List<Usuario> buscarUsuario(String unUsuario) {
		return (this.administradorDeUsuarios.buscarUsuariosPorNombre(unUsuario));
	}
	
	public Usuario buscarUsuarioPorId(String unId){
		return this.administradorDeUsuarios.buscarUsuarioPorId(unId);
	}

	// Proposito: Devuelve el perfil de un usuario particular. Si no lo encuentra, devuelve un profile vacio.
	public ProfileUsuario elegirUsuario(String unNombre) {
		return(administradorDeUsuarios.buscarUsuarioDeNick(unNombre));
	}

	// Proposito: Agrega una pelicula al sistema.
	public void agregarPelicula(Pelicula unaPelicula) {
		this.administradorDePeliculas.agregarElemento(unaPelicula);
	}

	// Proposito: Devuelve True si la pelicula esta integrada al sistema.
	public boolean tieneRegistradoAPelicula(Pelicula unaPelicula) {
		return (this.administradorDePeliculas.tieneRegistradoAElemento(unaPelicula));
	}

	// Proposito: Devuelve una lista de posibles resultados de una busqueda por nombre. Si no lo encuentra, devuelve un resultado vacio.
	public List<Pelicula> buscarPelicula(String unaPelicula) {
		return (this.administradorDePeliculas.buscarPeliculasPorNombre(unaPelicula));
	}

	// Proposito: Devuelve la informacion publica de una pelicula particular. Si no la encuentra, el resultado estara vacio.
	public PublicInfo elegirPelicula(String unaPelicula) {
		return(administradorDePeliculas.getInfoPublicaDelaPeliDeNombre(unaPelicula));
	}

	//Proposito: Devuelve una nueva instancia de reproduccion de una pelicula
	//Precondicion: La pelicula se tiene que haber buscado primero, por lo que siempre se va a
	//encontrar en el sistema.
	public Reproduccion verPelicula(Pelicula unaPelicula) {
		return (unaPelicula.getReproduccion());
	}

	//Proposito: Cuando un usuario le indica al sistema que quiere darle play a una reproduccion de una pelicula, el sistema la reproduce hasta el final y guarda 
	// a travez del administrador de usuarios y el administrador de peliculas, que el usuario vio la pelicula.
	//Precondicion: La reproduccion tiene que ser de una pelicula que esta en el sistema.
	public void darlePlayAReproduccion(Reproduccion unaReproduccion, Usuario unUsuario) {
		unaReproduccion.reproducirHastaElFinal();
		this.administradorDeUsuarios.agregarReproduccionDePeliculaAUsuario(unaReproduccion.getPelicula(), unUsuario);
		this.administradorDePeliculas.agregarReproduccionDeUsuarioAPelicula(unaReproduccion.getPelicula(), unUsuario);
	}

	//Proposito: Reproduce la pelicula hasta el minuto indicado. Si excede la duracion de la pelicula, no hace nada.
	public void darlePlayHastaMinuto(Reproduccion unaReproduccion, Integer unMinuto) {
		unaReproduccion.reproducirHastaMinuto(unMinuto);
	}

	//Proposito: Retorna si el usuario vio la pelicula.
	//precondicion: El usuario tiene que estar en el sistema.
	public boolean usuarioVioLaPelicula(Usuario unUsuario, Pelicula unaPelicula) {
		return (this.administradorDeUsuarios.usuarioVioLaPelicula(unUsuario,unaPelicula));
	}
	
	//Proposito: Logea al usuario 
	public void logear(Identificacion identificar) {
		this.iniciarSesion.logear(identificar);
		
	}
	
	//Proposito:Denota True si el usuario esta conectado al sistema
	//Caso contrario false
	public boolean seEncuentraConectado(String unNick) {
		
		return this.iniciarSesion.seEncuentraConectado(unNick);
	}
	
	//Proposito:Desconecta al usuario del sistema
	public void desconectar(String unNick) {
		this.iniciarSesion.desconectar(unNick);
		
	}
	
	//Proposito:Integra un listado de usuarios a iniciarSesion.
	public void integrarUsuariosRegistrados(ArrayList<Usuario> usuarios) {
		this.iniciarSesion.integrarUsuariosRegistrados(usuarios);
		
	}

	//Proposito: Guarda la calificacion que el usuario dio a la pelicula. 
	//En la pelicula guarda la lista de todas las calificaciones recibidas y en el usuario guarda la lista de todas las calificaciones dadas.
	//Si el usuario da una calificacion no valida, el sistema no registra nada.
	//Si el usuario no vio la pelicula, el sistema no registra nada.
	public void calificarPelicula(Usuario unUsuario, Pelicula unaPelicula, Integer unRating, String unReview) {
		if (this.usuarioVioLaPelicula(unUsuario, unaPelicula)&& unRating>=1 && unRating<=5){
		 Calificacion unaCalificacion = new Calificacion(unUsuario.getId().toString(), unaPelicula.getId(), unRating, unReview);
		 this.administradorDeUsuarios.agregarCalificacionAUsuario(unUsuario,unaCalificacion);
		 this.administradorDePeliculas.agregarCalificacionAPelicula(unaPelicula,unaCalificacion);
		}
		
	}

		
	
	//Proposito: Denota si un Usuario no hizo calificaciones.
	public boolean usuarioNoTieneCalificaciones(Usuario unUsuario) {
		return this.administradorDeUsuarios.usuarioNoTieneCalificaciones(unUsuario);
	}

	//Proposito: Denota si un usuario en particular pertenece a uno de los 10 con mayor cantidad de calificaciones
	public boolean tieneEnLosUsuariosQueRealizaronElMayorNumeroDeCalificacionesA(Usuario unUsuario) {
		return this.administradorDeUsuarios.tieneEnLosUsuariosQueRealizaronElMayorNumeroDeCalificacionesA(unUsuario) ;
	}
	
	


	//proposito: Dado un id de una pelicula, Rerotrna si el usuario la evaluo a esa pelicula.
	public boolean usuarioEvaluoLaPeliculaDeId(Usuario unUsuario, String idDePelicula) {
		return this.administradorDeUsuarios.usuarioEvaluoLaPeliculaDeID(unUsuario, idDePelicula);
	}

	//Proposito: Retorna el rating promedio que tienen las peliculas en el sistema
	public Integer getRatingPromedio() {
		return administradorDePeliculas.getRatingPromedio();
	}

	//Proposito: Retorna la lista de peliculas que tiene un rating mayor al numero dado.
	public ArrayList<Pelicula> getPeliculasConRatingMayorA(Integer unRating) {
		return administradorDePeliculas.getPeliculasConRatingMayorA(unRating);
	}

	public void recomendarUsuario(Usuario unUsuario) {
		unUsuario.getRecomendador().recomendarUsuario(unUsuario, this);
	}

	
	//Protocolo de interaccion con sistema de Importacion
	
	public void setRutaPeliculas(String unaRuta){
		this.sistemaDeImportacion.setRutaPeliculas(unaRuta);
	}
	public void setRutaUsuarios(String unaRuta){
		this.sistemaDeImportacion.setRutaUsuarios(unaRuta);
	}
	public void setRutaAmistades(String unaRuta){
		this.sistemaDeImportacion.setRutaAmistades(unaRuta);
	}
	public void setRutaCalificaciones(String unaRuta){
		this.sistemaDeImportacion.setRutaCalificaciones(unaRuta);
	}
	
	 
	public void importarPeliculas(){
		List<Pelicula> coleccionDePeliculas = sistemaDeImportacion.importarPeliculas();
			
		administradorDePeliculas.importarColeccionDePeliculas(coleccionDePeliculas);
	}
		
	public void importarUsuarios(){
		List<Usuario> coleccionDeUsuarios = sistemaDeImportacion.importarUsuarios();
			
		administradorDeUsuarios.importarColeccionDeUsuarios(coleccionDeUsuarios);
	}
		
	//Para que una calificacion sea valida de introducir en el sistema, El id del usuario y el id de la pelicula importados tienen que ser validos, osea, estar en el sistema.
	public List<Calificacion> importarCalificaciones(){
		List<Calificacion> coleccionDeCalificaciones = sistemaDeImportacion.importarCalificaciones().stream().filter(c -> this.esCalificacionValida(c)).collect(Collectors.toList());
	
		administradorDeUsuarios.importarCalificacionDeUsuarios(coleccionDeCalificaciones);
		administradorDePeliculas.importarCalificacionDePeliculas(coleccionDeCalificaciones);
		
		return coleccionDeCalificaciones;
	}
		

	public void importarAmistad(){
		List<Amistad> colleccionDeUsuarios=sistemaDeImportacion.importarAmistad();
		for(Amistad unaAmistad:colleccionDeUsuarios){
			Usuario emisor= this.administradorDeUsuarios.buscarUsuarioPorId(unaAmistad.getIdEmisor());
			Usuario reseptor=this.administradorDeUsuarios.buscarUsuarioPorId(unaAmistad.getIdReceptor());
			if (emisor!=null && reseptor!=null){
				reseptor.establecerAmistad(emisor);	
			}
			
				
		}
	}
		
		private Boolean esCalificacionValida(Calificacion unaCalificacion) {
			Boolean validezDePelicula= this.administradorDePeliculas.esIdDePeliculaValido(unaCalificacion.getIdPelicula());
			Boolean validezDeUsuario= this.administradorDeUsuarios.esIdDeUsuarioValido(unaCalificacion.getIdUsuario());
			return (validezDePelicula && validezDeUsuario);
		}

		public ArrayList<Calificacion> getCalificacionesDeUsuarios(List<Usuario> amigosDelUsuario) {
			return this.administradorDeUsuarios.getCalificacionesDeUsuarios(amigosDelUsuario);
		}
}
