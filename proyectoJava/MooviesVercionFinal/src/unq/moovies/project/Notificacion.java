package unq.moovies.project;

//responsabilidad: son msj de texto que se guardan en el usuario y le llegan
//					a travez del sistema cuando hace falta que este le comunique algo,
//					por ejemplo que se agrego una pelicula a la que estaba suscripta.
public class Notificacion {
		
	//Estructura
	
	private String mensaje;
	
	//Constructores

	public Notificacion(String unTexto) {
		super();
		this.mensaje=unTexto;
	}
	
	//Getters
	public String getMensaje() {
		return this.mensaje;
	}

}
