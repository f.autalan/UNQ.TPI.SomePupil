package unq.moovies.project;

import java.util.ArrayList;

import unq.moovies.Genero.Genero;
//Responsabilidad:Es la representacion de las peliculas que mooovis maneja.
public class Pelicula  implements Identificable{

	//Estructura
	private PublicInfo 		publicInfo;
	private Reproduccion 	reproduccion;
	private Genero 			genero;
	
	//Constructores
	public Pelicula(PublicInfo informacionPublica){
		this.setPublicInfo(informacionPublica);
	}
	
	public Pelicula(PublicInfo informacionPublica,Genero unGenero,Reproduccion unaReproduccion){

		this.genero=unGenero;
		this.setPublicInfo(informacionPublica);
		this.setReproduccion(unaReproduccion.setPelicula(this));
	}
	//Getters

	// La reproduccion que guarda la pelicula es un template, no queres a darle a los usuarios LA reproduccion, queres darle una iteracion de esa reproduccion, asi que la copias y la devolves.
	
	public Reproduccion getReproduccion() {
		Reproduccion reproduccionDeEstaPelicula = reproduccion;
		return (reproduccionDeEstaPelicula);
	}
	
	public Integer getDuracion() {
		return this.reproduccion.getDuracion();
	}

	
	public PublicInfo getPublicInfo() {
		return (this.publicInfo);
	}

	public String getId() {
		return this.publicInfo.getIdentificadorEnIMDB();
	}

	public String getFechaDeEstreno() {
		return this.publicInfo.getFechaDeEstreno();
	}
	
	public String getNombre() {
		return (this.publicInfo.getNombre());
	}
	public ArrayList<String> getGeneros(){
		return this.publicInfo.getGeneros();
	}
	public String getDirector(){
		return this.publicInfo.getDirector();
		
	}

	//Setters
	

	private void setPublicInfo(PublicInfo informacionPublica) {
		this.publicInfo = informacionPublica;
	}
	
	private void setReproduccion(Reproduccion unaReproduccion) {
		this.reproduccion  = unaReproduccion;
	}

	//Protocolo

	//Proposito: Denota si la pelicula es del genero dado.
	public boolean esDelGenero(String unGenero) {
		return this.publicInfo.getGeneros().contains(unGenero);
		
	}

	public Integer getIDInteger() {
		Integer resultado = Integer.parseInt(this.getId());
		return resultado;
	}

	public void notificarExtreno() {
		this.genero.notificar(this.notifacion());
		
	}

	private Notificacion notifacion() {
		Notificacion unaNotificacion=new Notificacion("Se extreno la Pelicula" + this.publicInfo.getNombre());
		return unaNotificacion;
	}

	
}
