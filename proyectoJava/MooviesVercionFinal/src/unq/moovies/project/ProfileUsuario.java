package unq.moovies.project;


//Representacion:Es la informacion que es visible para todos los usarios.
public class ProfileUsuario {
	
	
	private String Nombre;
	private String Apellido;
	private String Edad;
	private String CodigoPostal;
	private String Ocupacion;

	
	
	//Contructor
	public ProfileUsuario() {
		super();
			
	}
	
	public ProfileUsuario(String unNombre, String unApellido, String unaEdad, String unCodigoPostal, String unaOcupacion) {
		this();
		
		this.setNombre(unNombre);
		this.setApellido(unApellido);
		this.setEdad(unaEdad);
		this.setCodigoPostal(unCodigoPostal);
		this.setOcupacion(unaOcupacion);
		
			
	}
	
	//Getters
	public String getNombre() {
		return Nombre;
	}
	
	public String getApellido() {
		return this.Apellido;
	}
	
	public String getEdad() {
		return Edad;
	}
	
	public String getCodigoPostal() {
		return this.CodigoPostal;
	}
		
	public String getOcupacion() {
		return this.Ocupacion;
	}
	
	
	//Setters
	private void setNombre(String nombre) {
		this.Nombre = nombre;
	}

	private void setApellido(String apellido) {
		this.Apellido = apellido;
	}
		
	private void setEdad(String edad) {
		this.Edad = edad;
	}
	
	private void setOcupacion(String ocupacion) {
		this.Ocupacion = ocupacion;
	}
	
	private void setCodigoPostal(String codigoPostal) {
		this.CodigoPostal = codigoPostal;
	}
			
	
}
