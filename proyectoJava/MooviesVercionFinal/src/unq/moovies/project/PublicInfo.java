package unq.moovies.project;

import java.util.ArrayList;

//Representacion: Es la informacion que es visible para todos los usuarios.
public class PublicInfo {

	//Estructura
	
	private String nombre;
	private String fechaDeEstreno;
	private String identificadorEnIMDB;
	private ArrayList<String> generos;
	private String director;
	//Constructores
	

	
	//Contructor
	public PublicInfo() {
		super();
			
	}
	
	public PublicInfo(String unNombre, String unAnioDeEstreno, String unID, ArrayList<String> unaListaDeGeneros) {
		this();
		
		this.setNombre(unNombre);
		this.setFechaDeEstreno(unAnioDeEstreno);
		this.setIdentificadorEnIMDB(unID);
		this.setGeneros(unaListaDeGeneros);
	}
	
	public PublicInfo(String unNombre, String unAnioDeEstreno, String unID, ArrayList<String> unaListaDeGeneros, String director) {
		this();
		
		this.setNombre(unNombre);
		this.setFechaDeEstreno(unAnioDeEstreno);
		this.setIdentificadorEnIMDB(unID);
		this.setGeneros(unaListaDeGeneros);
		this.setDirector(director);
	}
	
	//Getters
	
	public String getNombre() {
		return this.nombre;
	}
	
	public String getFechaDeEstreno() {
		return this.fechaDeEstreno;
	}
	
    public String getIdentificadorEnIMDB() {
		return this.identificadorEnIMDB;
	}

    public ArrayList<String> getGeneros() {
		return this.generos;
	}	
    
    public String getDirector() {
		return this.director;
	}
	//Setters
	
	private void setNombre(String unNombre) {
		this.nombre= unNombre;
	}

	private void setFechaDeEstreno(String fechaDeEstreno) {
		this.fechaDeEstreno = fechaDeEstreno;
	}

	private void setIdentificadorEnIMDB(String identificadorEnIMDB) {
		this.identificadorEnIMDB = identificadorEnIMDB;
	}

	private void setGeneros(ArrayList<String> generos) {
		this.generos = generos;
	}
	
	private void setDirector(String unDirector) {
		this.director = unDirector;
	}
}
