package unq.moovies.project;

//Responsabilidad:	Representa una reproduccion especifica de una pelicula.
public class Reproduccion {
	
	//Estructura
	
	private Integer 	duracionEnMinutos;
	private Integer 	minutoActual;
	private Pelicula 	pelicula;
	
	//Constructores
	
	public Reproduccion(){
		super();
		this.setduracionEnMinutos(0);
		this.setminutoActual(0);
	} 
	
	public Reproduccion(Integer unaDuracion){
		this();
		this.setduracionEnMinutos(unaDuracion);
	}
	
	//Getters
	public Integer getDuracion() {
		return (this.duracionEnMinutos);
	}
	
	public Integer getMinutoActual() {
		return (this.minutoActual);
	}
	
	public Pelicula getPelicula() {
		return(this.pelicula);
	}
	
	//Setters
	
	private void setminutoActual(Integer unMinuto) {
		this.minutoActual=unMinuto;
		
	}
	private void setduracionEnMinutos(Integer unaDuracion) {
		this.duracionEnMinutos=unaDuracion;
	}

	public Reproduccion setPelicula(Pelicula unaPelicula) {
		this.pelicula=(unaPelicula);
		return (this);
	}

	
	//Protocolo

	//Proposito: Reproduce la pelicula hasta el final
	public void reproducirHastaElFinal() {
		this.setminutoActual(this.duracionEnMinutos);
	}
    
	//Proposito: Devuelve si la reproduccion ya esta al final
	public boolean reproduccionterminada() {
		return( this.minutoActual.equals( this.duracionEnMinutos));
	}

	//Proposito: Reproduce la reproduccion hasta el minuto indicado
	public void reproducirHastaMinuto(Integer unMinuto) {
		if (unMinuto<= duracionEnMinutos){
		 this.setminutoActual(unMinuto);	
		}
	}
	
}
