package unq.moovies.project;

import java.util.ArrayList;
import java.util.List;

import unq.moovies.sistemaDeBusqueda.Filtrable;
import unq.moovies.sistemaDeRecomendacion.MetodoDeRecomendacion;
import unq.moovies.sistemaDeRecomendacion.Recomendador;
import unq.moovies.sistemaInicioDeSesion.Identificacion;

//Responsabilidad: Representa la cuenta a travez de la cuel el individuo
//					interactura con el sistema.
public class Usuario implements Identificable{

	//Estructura
	
	private String nick;
	private String contrasenia;
	private ProfileUsuario profile;
	private List<Usuario> coleccionDeAmigos;
	private String id;
	private Recomendador recomendador;
	private List<Notificacion> coleccionDeNotificaciones;
	//Constructores
	public Usuario(String unNick,String unaContrasenia, ProfileUsuario unProfile, String unId){
		
		this.setNick(unNick);
		this.setContrasenia(unaContrasenia);
		this.setProfile(unProfile);
		this.setId(unId);
		this.setColeccionDeAmigos(new ArrayList <Usuario>());
		this.setColeccionDeNotificaciones(new ArrayList<Notificacion>());
		this.recomendador= new Recomendador();
		
	}
	
	

	//Getters
	public String getNombre(){
		return this.profile.getNombre();
	}
	
	public String getNick(){
		return (this.nick);
	}
	
	public String getContrasenia() {
		return contrasenia;
	}
	
	public ProfileUsuario getProfile() {
		return (this.profile);
	}
	
	public List<Usuario> getColeccionDeAmigos() {
		return (this.coleccionDeAmigos);
	}
	
	
	public String getId() {
		return this.id;
	}
	
	public List<Notificacion> getColeccionDeNotificaciones() {
		return this.coleccionDeNotificaciones;
	}
	
	public Recomendador getRecomendador() {
		return this.recomendador;
	}
	
	
	//Setters
	private void setNick(String unNick){
		this.nick = unNick;
	}
	
	private void setContrasenia(String unaContrasenia) {
		this.contrasenia = unaContrasenia;
	}
	

	private void setProfile(ProfileUsuario unProfile) {
		this.profile = unProfile;
	}
	
	public void setId(String unId){
		this.id = unId;
	}
	
	public void setColeccionDeNotificaciones(List<Notificacion> unaColeccionDeNotificaciones) {
		this.coleccionDeNotificaciones= unaColeccionDeNotificaciones;
		
	}
	
	
	//PRoposito:Setea la coleccion de amigos
	//Precondicion:---
	public void setColeccionDeAmigos(List <Usuario> coleccionAmigos) {
		this.coleccionDeAmigos = coleccionAmigos;
	}

	
	//Proposito: Logea al usuario 
	public void iniciarSesion(Moovies moovies){
		moovies.logear(this.identificar());
		
	}

	//Proposito: Denota True si el usuario esta conectado al sistema
	//Caso contrario false
	public boolean estoyConectado(Moovies moovies) {
		return moovies.seEncuentraConectado(this.getNick());
	}

	//Proposito: Desconecta al usuario del sistema
	public void desconectarse(Moovies moovies) {
		moovies.desconectar(this.getNick());
		
	}

	

	//Proposito: Crea un indentificador para el usuario el cual nesesita nick y contrasenia 
	//del propio usuario
	public Identificacion identificar() {
		String nick=this.getNick();
		String contrasenia=this.getContrasenia();
		Identificacion identifiacion= new Identificacion(nick,contrasenia);
		return identifiacion;
	}
	
	//proposito:agrega un usuario a la colecccion de amigos
	//Proposito:--
	public void addAmigo(Usuario user){
		coleccionDeAmigos.add(user);
	}
	
	//Proposito:borra un amigo de la coleccion de amigos
	//Precondicion:
	public void removeAmigo(Usuario unUsuario) {
		this.coleccionDeAmigos.remove(unUsuario);
		
	}

	//Proposito:entablece amistad entre dos usuarios.
	//Precondicion:---
	public void establecerAmistad(Usuario user) {
		this.addAmigo(user);
		user.addAmigo(this);
	}
	
	//Proposito:Borra una amistad
	//Precondicion:-
	public void borrarAmistad(Usuario unUsuario) {
		this.removeAmigo(unUsuario);
		unUsuario.removeAmigo(this);
		
	}

	
	
	
	
	public List<Pelicula> filtrar(Filtrable filtro, List<Pelicula> pelis){
		return filtro.ejecutarFiltrado(pelis);
	}


	
	
	//Proposito: El Usuario Elije una forma de recomendacion  y la agrega a su recomendador para recordarla mas tarde si cambia su opinion de como le gusta ser recomendado.
	public void agregarRecomendacion(MetodoDeRecomendacion unaRecomendacion) {
		this.recomendador.agregarRecomendacion(unaRecomendacion);
		
	}

	//Proposito: Permitirle al usuario elijir como moovies quiere recomendarle.
	public void setRecomendacion(int i) {
		this.recomendador.cambiarMetodoDeRecomendacion(i);
		
	}



	//Proposito:Agrega una Notifacion a la coleccion De notifiaciones
	public void agregarNotificacion(Notificacion unaNotificacion) {
		this.coleccionDeNotificaciones.add(unaNotificacion);
		
	}


	//Proposito:Elimina una Notifacion a la coleccion De notifiaciones
	public void borrarNotificacion(Notificacion unaNotificacion) {
		this.coleccionDeNotificaciones.remove(unaNotificacion);
		
	}

}