package unq.moovies.sistemaDeAdministradores;

import java.util.ArrayList;
//Responsabilidad: Guarda, maneja, ordena, y opera sobre un conjunto de 
//				   de elemento determinados
public abstract class Administrador<E> {

	//Estructura
	
	protected ArrayList<E> colleccionDeElementos; 
	
	//Protocolo
	
	public abstract void agregarElemento(E unElemento);

	// Devuelve si tiene registrado al elemento buscado
	public abstract Boolean tieneRegistradoAElemento(E unElemento);
}
