package unq.moovies.sistemaDeAdministradores;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import unq.moovies.project.Pelicula;
import unq.moovies.project.PublicInfo;
import unq.moovies.project.Usuario;
import unq.moovies.sistemaDeBusqueda.Campo;
import unq.moovies.sistemaDeBusqueda.Condicion;
import unq.moovies.sistemaDeBusqueda.Contiene;
import unq.moovies.sistemaDeBusqueda.EsIgual;
import unq.moovies.sistemaDeBusqueda.FiltradoSimple;
import unq.moovies.sistemaDeBusqueda.Id;

import unq.moovies.sistemaDeBusqueda.Nombre;
import unq.moovies.sistemaDeCalificacion.Calificacion;


//Responsabilidad: Guarda, maneja, ordena, y opera sobre un conjunto de 
//				   de peliculas. 
//				   para esto hace uso de diferentes filtros.
//				   tambien sabe cuales son las calificaciones que le hicieron
//				   a cada pelicula y que usario las vio;
//				   lo cual no es esencial al pelicula pero el sistema
//				   nesesita saber y se encuentra dentro de la esencia de un 
// 				   administrador.

public class AdministradorDePeliculas extends Administrador<Pelicula> {


	//Estructura

	private HashMap<Pelicula,ArrayList<Usuario>> usuariosQueVieronCadaPelicula;
	private HashMap<Pelicula,ArrayList<Calificacion>> calificacionesDeCadaPelicula;
	
	//Constructores
	
	public AdministradorDePeliculas(){
		super();
		this.colleccionDeElementos = new ArrayList<Pelicula>();
		this.usuariosQueVieronCadaPelicula = new HashMap<Pelicula,ArrayList<Usuario>>();
		this.calificacionesDeCadaPelicula = new HashMap<Pelicula,ArrayList<Calificacion>>();
	}
	
	//Getters

	public ArrayList<Pelicula> getPeliculasConId(List<String> idPeliculas) {
		ArrayList<Pelicula> coleccionDePeliculas = new ArrayList<Pelicula>();
		for (String idPelicula: idPeliculas) {
			Pelicula unaPelicula = this.colleccionDeElementos.stream().filter(pelicula-> pelicula.getId().equals(idPelicula)).collect(Collectors.toList()).get(0);
			coleccionDePeliculas.add(unaPelicula);
		}
		return coleccionDePeliculas;
	}
	
	//Precondicion:La pelicula buscada tiene que ser parte del sistema.
	public PublicInfo getInfoPublicaDelaPeliDeNombre(String unaPelicula) {

		Condicion unaCondicion= new EsIgual();
		Campo unCampo= new Nombre();
		
		FiltradoSimple filtro = new FiltradoSimple(unCampo,unaPelicula,unaCondicion);
		List<Pelicula> listaFiltrada= filtro.ejecutarFiltrado(this.colleccionDeElementos);
		
		PublicInfo elementoBuscado = (listaFiltrada.get(0).getPublicInfo());
		return(elementoBuscado);
	}
	
	public ArrayList<Calificacion> getCalificacionesDePelicula(Pelicula unaPelicula) {
		return this.calificacionesDeCadaPelicula.get(unaPelicula) ;
	}

	public Integer getCalificacionPromedioDePelicula(Pelicula unaPelicula) {
		Integer resultado = 0;
		Integer contador = 0;
		
		if(this.peliculaNoTieneCalificaciones(unaPelicula)){
			return(0);
		}
		for (Calificacion unaCalificacion: (this.calificacionesDeCadaPelicula.get(unaPelicula))){
			resultado= resultado+ unaCalificacion.getRating();
			contador= contador+1;
		}
		
		 return(resultado/contador);
	}

	public ArrayList<Pelicula> get10UsuariosConMayorCalificacionesDescendente() {
		ArrayList<Pelicula> resultado = this.colleccionDeElementos;
		resultado.sort((Pelicula p1,Pelicula p2) -> (this.getCalificacionPromedioDePelicula(p2)).compareTo(this.getCalificacionPromedioDePelicula(p1)));
		if (resultado.size()> 10) {
			resultado.subList(0, 11);
			return (resultado);
		}
		return (resultado);
		
	}

	
	//Setters
	
	//Protocolo

	//Proposito: Agrega una pelicula a la coleccion administrada, y a los modulos de los usuarios que vieron esa pelicula y las calificaciones de esa pelicula.
	public void agregarElemento(Pelicula unaPelicula) {
		this.colleccionDeElementos.add(unaPelicula);
		this.usuariosQueVieronCadaPelicula.put(unaPelicula, new ArrayList<Usuario>());
		this.calificacionesDeCadaPelicula.put(unaPelicula, new ArrayList<Calificacion>());
	}
	

	//Proposito: Agrega a una pelicula, que un usuario vio esa pelicula.
	//Precondicion: La pelicula y El usuario tienen que estar registrados en el sistema.
	public void agregarReproduccionDeUsuarioAPelicula(Pelicula unaPelicula, Usuario unUsuario) {
		this.usuariosQueVieronCadaPelicula.get(unaPelicula).add(unUsuario);
	}

	//Proposito: Agrega una calificacion a una pelicula
	//Precondicion: La pelicula calificada tiene que ser la pelicula dada y el author de la calificacion tiene que estar registrado en el sistema
	public void agregarCalificacionAPelicula(Pelicula unaPelicula, Calificacion unaCalificacion) {
		this.calificacionesDeCadaPelicula.get(unaPelicula).add(unaCalificacion);
	}
	
	//Proposito: Devuelve si una pelicula dada no tiene calificaciones todavia.
	//Precondicion: la pelicula tiene que estar registrada en el sistema
	public boolean peliculaNoTieneCalificaciones(Pelicula unaPelicula) {
		return(this.calificacionesDeCadaPelicula.get(unaPelicula).isEmpty());
	}

	public void importarColeccionDePeliculas(List<Pelicula> coleccionDePeliculas) {
		for (Pelicula unaPelicula: coleccionDePeliculas)
		{
			this.agregarElemento(unaPelicula);
		}
		
	}

	public Boolean esIdDePeliculaValido(String idPelicula) {
		Condicion unaCondicion= new EsIgual();
		Campo unCampo= new Id();
		
		FiltradoSimple filtro = new FiltradoSimple(unCampo,idPelicula,unaCondicion);
		List<Pelicula> listaFiltrada= filtro.ejecutarFiltrado(this.colleccionDeElementos);
		
		return ((listaFiltrada.isEmpty()) == false);
	}

	public void importarCalificacionDePeliculas(List<Calificacion> coleccionDeCalificaciones) {
		for (Calificacion unaCalificacion: coleccionDeCalificaciones) {
			Condicion unaCondicion= new EsIgual();
			Campo unCampo= new Id();
			
			FiltradoSimple filtro = new FiltradoSimple(unCampo,unaCalificacion.getIdPelicula(),unaCondicion);
			List<Pelicula> listaFiltrada= filtro.ejecutarFiltrado(this.colleccionDeElementos);
			Pelicula unaPelicula= listaFiltrada.get(0);
			
			this.calificacionesDeCadaPelicula.get(unaPelicula).add(unaCalificacion);
		}
	}

	public Integer getRatingPromedio() {
		Integer contador = 0;
		for (Pelicula unaPelicula: this.colleccionDeElementos) {
			contador= contador+this.getCalificacionPromedioDePelicula(unaPelicula);
		}
		return contador / (this.colleccionDeElementos.size());
	}

	public ArrayList<Pelicula> getPeliculasConRatingMayorA(Integer unRating) {
		ArrayList<Pelicula> peliculasConRatingMayor = new ArrayList<Pelicula>();
		
		for (Pelicula unaPelicula: this.colleccionDeElementos) {
			if (this.getCalificacionPromedioDePelicula(unaPelicula) > unRating) {
				peliculasConRatingMayor.add(unaPelicula);
			}
		}
		
		return peliculasConRatingMayor;
	}

	@Override
	public Boolean tieneRegistradoAElemento(Pelicula unaPelicula) {
		return this.colleccionDeElementos.contains(unaPelicula);
	}

	public List<Pelicula> buscarPeliculasPorNombre(String unaPelicula) {
		Condicion unaCondicion= new Contiene();
		Campo unCampo= new Nombre();
		
		FiltradoSimple filtro = new FiltradoSimple(unCampo,unaPelicula,unaCondicion);
		List<Pelicula> listaFiltrada= filtro.ejecutarFiltrado(this.colleccionDeElementos);
		
		return listaFiltrada;
	}

	
}
