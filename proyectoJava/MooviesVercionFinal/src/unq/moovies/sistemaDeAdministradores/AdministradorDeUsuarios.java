package unq.moovies.sistemaDeAdministradores;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import unq.moovies.project.Pelicula;
import unq.moovies.project.ProfileUsuario;
import unq.moovies.project.Usuario;
import unq.moovies.sistemaDeBusqueda.Campo;
import unq.moovies.sistemaDeBusqueda.Condicion;
import unq.moovies.sistemaDeBusqueda.Contiene;
import unq.moovies.sistemaDeBusqueda.EsIgual;
import unq.moovies.sistemaDeBusqueda.FiltradoSimple;
import unq.moovies.sistemaDeBusqueda.Id;
import unq.moovies.sistemaDeBusqueda.Nick;
import unq.moovies.sistemaDeBusqueda.Nombre;
import unq.moovies.sistemaDeCalificacion.Calificacion;

//Responsabilidad:  Guarda, maneja, ordena, y opera sobre un conjunto de 
//					de Usuarios. 
//					para esto hace uso de diferentes filtros.
//					tambien sabe cuales son las calificaciones que
//					que cada usuario hizo y que peliculas vio;
//					lo cual no es esencial al usuario pero el sistema
//					nesesita saber y se encuentra dentro de la esencia de un 
// 					administrador.
public class AdministradorDeUsuarios  extends Administrador<Usuario>  {

	//Estructura
	
	private HashMap<Usuario,ArrayList<Pelicula>> peliculasVistasPorCadaUsuario;
	private HashMap<Usuario,ArrayList<Calificacion>> calificacionesDadasPorCadaUsuario;
	
	//Constructores
	
	public AdministradorDeUsuarios(){
		super();
		this.colleccionDeElementos = new ArrayList<Usuario>();
		this.peliculasVistasPorCadaUsuario = new HashMap<Usuario,ArrayList<Pelicula>>();
		this.calificacionesDadasPorCadaUsuario = new HashMap<Usuario,ArrayList<Calificacion>>();
	}
	
	//Getters
	

	private Integer getCantidadDeCalificacionesDeUsuario(Usuario unUsuario) {
		return this.getCalificacionesDeUsuario(unUsuario).size();
	}

	public ArrayList<Calificacion> getCalificacionesDeUsuario(Usuario unUsuario) {
		return  this.calificacionesDadasPorCadaUsuario.get(unUsuario);
	}

	public Integer getCalificacionesPromedioDeUsuario(Usuario unUsuario) {
		Integer resultado = 0;
		Integer contador = 0;
		
		if (this.usuarioNoTieneCalificaciones(unUsuario)){
			return (0);
		}

		for (Calificacion unaCalificacion: (this.calificacionesDadasPorCadaUsuario.get(unUsuario))){
			resultado= resultado+ unaCalificacion.getRating();
			contador= contador+1;
		}
		 return(resultado/contador);
	}

	public ArrayList<Usuario> get10UsuariosConMayorCalificacionesDescendente() {
		ArrayList<Usuario> resultado = this.colleccionDeElementos;
		
		resultado.sort((Usuario p1,Usuario p2) -> (this.getCantidadDeCalificacionesDeUsuario(p2)).compareTo(this.getCantidadDeCalificacionesDeUsuario(p1)));		
		
		if (resultado.size()> 10) {
			resultado.subList(0, 11);
			return (resultado);
		}
		
		return (resultado);
	}
	
	//Setters
	
	//Protocolo

	//Si el dia de maniana quiero cambiar como implemento coleccion de usuarios, me aseguro que al cliente siempre le llegue lo que espera pasandole una Coleccion.
	
	@Override
	//Proposito: Agrega un Usuario  a la coleccion administrada, y al modulo de las peliculas que vieron esos usuarios y al de las calificaciones que dio ese usuario.
	public void agregarElemento(Usuario unUsuario) {
		this.colleccionDeElementos.add(unUsuario);
		this.peliculasVistasPorCadaUsuario.put(unUsuario, new ArrayList<Pelicula>());
		this.calificacionesDadasPorCadaUsuario.put(unUsuario, new ArrayList<Calificacion>());
	}
	
	//Proposito: Dado un nick, busca y devuelve ese usuario
	//Si no encuentra al usuario, el resultado va a ser un perfil vacio.
	public ProfileUsuario buscarUsuarioDeNick(String unNick) {
		ProfileUsuario elementoBuscado;
		
		Condicion unaCondicion= new EsIgual();
		Campo unCampo= new Nick();
		
		FiltradoSimple filtro = new FiltradoSimple(unCampo,unNick,unaCondicion);
		List<Usuario> listaFiltrada= filtro.ejecutarFiltrado(this.colleccionDeElementos);
		
		if (listaFiltrada.isEmpty()) {
			elementoBuscado= new ProfileUsuario();
		}
		else { 
			elementoBuscado =listaFiltrada.get(0).getProfile();
		}	
		return(elementoBuscado);
	}

	//Proposito:  Agrega a un usuario que ese usuario vio esa pelicula
	//Precondicion: El usuario y la pelicula tienen que estar registrados en el sistema.
	public void agregarReproduccionDePeliculaAUsuario(Pelicula unaPelicula, Usuario unUsuario) {
		 peliculasVistasPorCadaUsuario.get(unUsuario).add(unaPelicula);
	}

	//Proposito: Retorna si un usuario vio una pelicula
	//Precondicion: El usuario esta en el sistema
	public boolean usuarioVioLaPelicula(Usuario unUsuario, Pelicula unaPelicula) {
		return(peliculasVistasPorCadaUsuario.get(unUsuario).contains(unaPelicula));
	}
	
	//Proposito: Agrega una calificacion a un usuario
	//Precondicion: El usuario esta en el sistema, la calificacion tiene como author a ese usuario y hace referencia a una pelicula que esta en el sistema.
	public void agregarCalificacionAUsuario(Usuario unUsuario, Calificacion unaCalificacion) { 
		calificacionesDadasPorCadaUsuario.get(unUsuario).add(unaCalificacion);		
	}
	
	//Proposito: Denota si un usuario hizo calificaciones todavia
	//Precondicion: El usuario esta registrado en el sistema
	public boolean usuarioNoTieneCalificaciones(Usuario unUsuario) {
		return (this.calificacionesDadasPorCadaUsuario.get(unUsuario).isEmpty());
	}
	
	//Proposito: Denota si un usario esta entre los 10 usuarios que hicieron la mayor cantidad de calificaciones
	//Precondicion: El usuario esta registrado en el sistema
	public Boolean tieneEnLosUsuariosQueRealizaronElMayorNumeroDeCalificacionesA(Usuario unUsuario) {
		return this.get10UsuariosConMayorCalificacionesDescendente().contains(unUsuario);
	}

	public void importarColeccionDeUsuarios(List<Usuario> coleccionDeUsuarios) {
		for (Usuario unUsuario: coleccionDeUsuarios){
			this.agregarElemento(unUsuario);
		}
		
	}
	
	//Proposito: Busca al usuario por el id indicado. Si no lo encuentra, devuelve null.
	public Usuario buscarUsuarioPorId(String id){

		Condicion unaCondicion= new EsIgual();
		Campo unCampo= new Id();
		
		FiltradoSimple filtro = new FiltradoSimple(unCampo,id,unaCondicion);
		List<Usuario> listaFiltrada= filtro.ejecutarFiltrado(this.colleccionDeElementos);
		
		if (listaFiltrada.isEmpty()){
		 return null;
		}
		Usuario usuarioResultado= listaFiltrada.get(0);
		return usuarioResultado;
	}

	public Boolean esIdDeUsuarioValido(String idUsuario) {
		Condicion unaCondicion= new EsIgual();
		Campo unCampo= new Id();
		
		FiltradoSimple filtro = new FiltradoSimple(unCampo,idUsuario,unaCondicion);
		List<Usuario> listaFiltrada= filtro.ejecutarFiltrado(this.colleccionDeElementos);
		
		return (!(listaFiltrada.isEmpty()));
	}

	// Por cada calificacion, busca al usuario de la id de esa calificacion, y en esa llave agrega la calificacion. 
	// Precondicion: Todos los usuarios tienen que estar en la base de datos.
	public void importarCalificacionDeUsuarios(List<Calificacion> coleccionDeCalificaciones) {
		for (Calificacion unaCalificacion: coleccionDeCalificaciones) {
			Condicion unaCondicion= new EsIgual();
			Campo unCampo= new Id();
			
			FiltradoSimple filtro = new FiltradoSimple(unCampo,unaCalificacion.getIdUsuario(),unaCondicion);
			List<Usuario> listaFiltrada= filtro.ejecutarFiltrado(this.colleccionDeElementos);
			
			Usuario elUsuario= listaFiltrada.get(0);
			
			this.calificacionesDadasPorCadaUsuario.get(elUsuario).add(unaCalificacion);
		}
	}

	public boolean usuarioEvaluoLaPeliculaDeID(Usuario unUsuario, String idDePelicula) {
		return calificacionesDadasPorCadaUsuario.get(unUsuario).stream().anyMatch(calificacion-> calificacion.getIdPelicula().equals(idDePelicula));
	}

	@Override
	public Boolean tieneRegistradoAElemento(Usuario unElemento) {
		return this.colleccionDeElementos.contains(unElemento);
	}

	public List<Usuario> buscarUsuariosPorNombre(String unUsuario) {
		Condicion unaCondicion= new Contiene();
		Campo unCampo= new Nombre();
		
		FiltradoSimple filtro = new FiltradoSimple(unCampo,unUsuario,unaCondicion);
		List<Usuario> listaFiltrada= filtro.ejecutarFiltrado(this.colleccionDeElementos);
	
		
		return listaFiltrada;
	}

	public ArrayList<Calificacion> getCalificacionesDeUsuarios(List<Usuario> amigosDelUsuario) {
		ArrayList<Calificacion> resultado = new ArrayList<Calificacion>();
		for(Usuario unUsuario:amigosDelUsuario){
			
			resultado.addAll(this.getCalificacionesDeUsuario(unUsuario));
		}
		
		return resultado;
	}

}
