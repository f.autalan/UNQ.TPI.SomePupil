package unq.moovies.sistemaDeBusqueda;



//Responsabilidad:Reprensentan los campos a evaluar de un elemento.
public interface Campo<T> {
	//Protocolo
	public String getCampo(T elemento);
}
