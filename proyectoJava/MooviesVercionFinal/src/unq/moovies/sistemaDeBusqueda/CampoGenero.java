package unq.moovies.sistemaDeBusqueda;

import java.util.List;


import unq.moovies.project.Pelicula;

//Responsabilidad:Obtener el campo a evaluar del elemento.
public class CampoGenero implements Campo<Pelicula> {
	//Protocolo
	@Override
	public String getCampo(Pelicula unElemento) {
		String resultado= new String();
		List<String> coleccionDeGeneros = unElemento.getGeneros();
		for (String ungenero: coleccionDeGeneros) {
			resultado= resultado+ungenero;
		}
		return resultado;
	}

	
}
