package unq.moovies.sistemaDeBusqueda;


//Responsabilidad:Son las condiciones que tiene que cumplir los elementos que son 
//				  filtrados por un valor
public interface Condicion {
	//Protocolo
	public boolean cumpleConCondicion(String unValor,String otroValor);

}
