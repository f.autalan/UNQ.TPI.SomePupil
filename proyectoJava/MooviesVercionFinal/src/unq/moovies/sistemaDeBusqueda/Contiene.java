package unq.moovies.sistemaDeBusqueda;

//Responsabilidad:Evalua si un valor contiene al otro.
public class Contiene implements Condicion{

	//Protocolo
	public boolean cumpleConCondicion(String stringAComparar, String valor) {
		
		return stringAComparar.contains(valor);
	}

}
