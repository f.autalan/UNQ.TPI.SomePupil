package unq.moovies.sistemaDeBusqueda;



import unq.moovies.project.Pelicula;
//Responsabilidad:Obtener el campo a evaluar del elemento.
public class Director implements Campo<Pelicula> {
	//Protocolo
	@Override
	public String getCampo(Pelicula unElemento) {
		return unElemento.getDirector();
	}

	

}
