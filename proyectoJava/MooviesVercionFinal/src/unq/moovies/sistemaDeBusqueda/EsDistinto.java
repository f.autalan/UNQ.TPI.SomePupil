package unq.moovies.sistemaDeBusqueda;

//Responsabilidad:Evalua si  los valores son distintos entre si.
public class EsDistinto implements Condicion{
	
	//Protocolo
	public boolean cumpleConCondicion(String unValor, String otroValor) {
		
		return ! unValor.equals(otroValor);
	}

}
