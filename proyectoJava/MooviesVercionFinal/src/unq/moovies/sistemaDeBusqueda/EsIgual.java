package unq.moovies.sistemaDeBusqueda;


//Responsabilidad:Evalua si los valores son iguales
public class EsIgual implements Condicion {

		//Protocolo
		public boolean cumpleConCondicion(String unValor, String otroValor) {
			
			return unValor.equals(otroValor);
		}	
	

}
