package unq.moovies.sistemaDeBusqueda;
//Responsabilidad: Evalua si un valor es mayor o igual a otro
public class EsMayorIgual implements Condicion {

	@Override
	public boolean cumpleConCondicion(String unValor, String otroValor) {
		return Integer.parseInt(unValor)>=Integer.parseInt(otroValor);
	}

	
}
