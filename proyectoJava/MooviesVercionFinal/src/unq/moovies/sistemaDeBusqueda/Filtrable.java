package unq.moovies.sistemaDeBusqueda;

import java.util.List;
//Responsabilidad:Son objetos capaces de filtrar Elementos.
public interface Filtrable<E> {

	public  List<E> ejecutarFiltrado(List<E> unElemento);
}
