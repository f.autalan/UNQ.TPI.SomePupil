package unq.moovies.sistemaDeBusqueda;

import java.util.ArrayList;
import java.util.List;


//Responsabilidad:Filtrar conjuntivamente.
public class FiltradoAND<E> extends FiltradoCompuesto<E>{

	
	//Contrucctor
	public FiltradoAND (Filtrable filtro, Filtrable filtro2){
		this.setPrimerFiltro(filtro);
		this.setSegundoFiltro(filtro2);
	}

	
	//Protocolo
	@Override
	public List<E> ejecutarFiltrado(List<E> pelis) {
		List <E> result = new ArrayList <E>();
		result = this.getPrimerFiltro().ejecutarFiltrado(this.getSegundoFiltro().ejecutarFiltrado(pelis));
		return result;
		
	}
}	