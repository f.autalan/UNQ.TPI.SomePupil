package unq.moovies.sistemaDeBusqueda;


import java.util.List;



//Responsabilidad:Los Filtros compuestos son aquellos filtros que estan conformados por otros filtros.
public abstract class FiltradoCompuesto<E> implements Filtrable<E> {

	private Filtrable 				primerFiltro;
	private Filtrable 				segundoFiltro;
	
	//constructor
	public FiltradoCompuesto(){
		super();
		
	}

	//getters and setters
	public Filtrable getPrimerFiltro() {
		return primerFiltro;
	}
	
	public Filtrable getSegundoFiltro() {
		return segundoFiltro;
	}
	
	public void setPrimerFiltro(Filtrable filtro1) {
		this.primerFiltro = filtro1;
	}
	public void setSegundoFiltro(Filtrable filtro2) {
		this.segundoFiltro = filtro2;
	}
	
	
	//protocolo
	public abstract List <E> ejecutarFiltrado(List<E> pelis);

}