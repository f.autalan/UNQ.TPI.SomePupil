package unq.moovies.sistemaDeBusqueda;
import java.util.ArrayList;
import java.util.List;

//Responsabilidad:Filtrar disyuntivamente.
public class FiltradoOR<E> extends FiltradoCompuesto<E>{

	
	//Contrucctor
	public FiltradoOR (Filtrable<E> filtro, Filtrable<E> filtro2){
		this.setPrimerFiltro(filtro);
		this.setSegundoFiltro(filtro2);
		
	}
	
	//Protocolo
	@Override
	public List<E> ejecutarFiltrado(List<E> pelis) {
		List <E> result = new ArrayList <E>();
		result = this.getPrimerFiltro().ejecutarFiltrado(pelis);
		result.addAll(this.getSegundoFiltro().ejecutarFiltrado(result));
		return result;
	}
}