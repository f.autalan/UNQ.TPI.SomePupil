package unq.moovies.sistemaDeBusqueda;

import java.util.List;
import java.util.stream.Collectors;

//Responsabilidad:Filtrar una lista de elementos por un campo una condicion y un valor que el cliente decide utilizar.
public class FiltradoSimple<E> implements Filtrable<E> {

	protected Campo campo;
	protected String valor;
	protected Condicion condicion;
	
	//constructor	
	public FiltradoSimple(Campo campo, String valor, Condicion condicion){
		this.campo = campo;
		this.valor = valor;
		this.condicion = condicion;
	}
	
	
	//Protocolo
	public List<E> ejecutarFiltrado(List<E> unaColeccionDeElementos){
		return unaColeccionDeElementos.stream().filter(elemento-> condicion.cumpleConCondicion(campo.getCampo(elemento), this.valor)).collect(Collectors.toList());
	};
		
}
