package unq.moovies.sistemaDeBusqueda;

//Responsabilidad:Obtener el campo a evaluar del elemento.
import unq.moovies.project.Identificable;

public class Id implements Campo<Identificable> {

	//Proposito
	@Override
	public String getCampo(Identificable unElemento) {
		return unElemento.getId();
	}


}
