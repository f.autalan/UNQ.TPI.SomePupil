package unq.moovies.sistemaDeBusqueda;




import unq.moovies.project.Identificable;
//Responsabilidad:Obtener el campo a evaluar del elemento.
public class Nombre implements Campo<Identificable>{
	
	//Protocolo
	@Override
	public String getCampo(Identificable unElemento) {
		return unElemento.getNombre();
	}

	




}
