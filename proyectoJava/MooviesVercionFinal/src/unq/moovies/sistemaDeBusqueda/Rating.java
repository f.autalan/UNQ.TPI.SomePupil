package unq.moovies.sistemaDeBusqueda;

import unq.moovies.sistemaDeCalificacion.Calificacion;

//Responsabilidad: obtener el campo a evaluar del elemento
public class Rating implements Campo<Calificacion>{

	@Override
	public String getCampo(Calificacion unaCalificacion) {
		
		return unaCalificacion.getRating().toString();
	}

}
