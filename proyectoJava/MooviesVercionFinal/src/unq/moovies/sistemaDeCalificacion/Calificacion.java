package unq.moovies.sistemaDeCalificacion;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

//Responsabilidad:Representa a una calificacion de un usuario a una pelicula.
public class Calificacion {

	//Estructura
	private String author;
	private String peliculaCalificada;
	private Integer rating;
	@SuppressWarnings("unused")
	private String review;
	private String fecha;
	//Constructores
	
	public Calificacion(){
		super();
	}
	
	//Utilizado en la generacion de calificaciones por el sistema
	public Calificacion(String idDeUsuario, String idDePelicula, Integer unRating, String unReview) {
		this();
		DateTimeFormatter dateformat = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		this.setfecha(dateformat.format(now));
		this.setAuthor(idDeUsuario);
		this.setPeliculaCalificada(idDePelicula);
		this.setRating(unRating);
		this.setReview(unReview);
	}

	//Utilizado por la generacion de calificaciones en el file reader, Cambia por que asigna su propio timeStamp en vez de crearlo en el momento.
	

	public Calificacion(String idDeUsuario, String idDePelicula,String unTimeStamp, Integer unRating) {
		this();
		this.setAuthor(idDeUsuario);
		this.setPeliculaCalificada(idDePelicula);
		this.setRating(unRating);
		this.setReview("vacio");
		this.setfecha(unTimeStamp);
	}
	
	//Getters
	
	public Integer getRating() {
		return this.rating;
	}
	
	public String getIdUsuario() {
		return this.author;
	}

	public String getIdPelicula() {
		return this.peliculaCalificada;
	}
	
	public String getTimeStamp() {
		return this.fecha;
	}
	
	//Setters
	

	private void setfecha(String unaFecha) {
		this.fecha= unaFecha;
	}
	
	private void setReview(String unReview) {
		this.review= unReview;
		
	}

	private void setRating(Integer unRating) {
		this.rating=unRating;
		
	}

	private void setPeliculaCalificada(String idDePelicula) {
		this.peliculaCalificada=idDePelicula;
		
	}

	private void setAuthor(String idDeUsuario) {
		this.author= idDeUsuario;
		
	}
}
