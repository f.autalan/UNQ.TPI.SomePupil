package unq.moovies.sistemaDeRecomendacion;

import java.util.List;

import unq.moovies.project.Moovies;
import unq.moovies.project.Notificacion;
import unq.moovies.project.Pelicula;
import unq.moovies.project.Usuario;

//Responsabilidad: Son las diferentes estrategias de recomendacion.
public abstract class MetodoDeRecomendacion {
	
	//Protocolo
	public  abstract void recomendarUsuario(Usuario unUsuario, Moovies unMoovies);
	
	public void crearNotificacion(List<Pelicula> peliculasRecomendadas,Usuario unUsuario){
		String textoDeRecomendacion = "Moovies aprecia tu subscripcion y conociendote como nadie mas, esta temporada te recomienda: ";
		for (Pelicula unaPelicula: peliculasRecomendadas) {
			textoDeRecomendacion = textoDeRecomendacion+unaPelicula.getNombre()+", ";
		
		}
		Notificacion notificacion= new Notificacion(textoDeRecomendacion);
		unUsuario.agregarNotificacion(notificacion);
	}

}
