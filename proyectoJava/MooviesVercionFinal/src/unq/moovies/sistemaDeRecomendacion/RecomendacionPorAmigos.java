package unq.moovies.sistemaDeRecomendacion;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import unq.moovies.project.Moovies;

import unq.moovies.project.Pelicula;
import unq.moovies.project.Usuario;


//Responsabilidad: Recomendar a un usuario por las peliculas
//				   que mas de la mitad de sus amigos
//				   evaluron con 4 o mas y el todavia no evaluo.
public class RecomendacionPorAmigos extends RecomendadorConApariciones {

	//Constructor
	public RecomendacionPorAmigos() {
		super();
	}
	
	//Protocolo
	public void recomendarUsuario(Usuario unUsuario, Moovies unMoovies) {

			List<Usuario> amigosDelUsuario= unUsuario.getColeccionDeAmigos();
			this.agregarCalificacionesDeAmigosAApariciones(unMoovies, amigosDelUsuario, "4");

			Integer mitadDeAmigos= amigosDelUsuario.size()/2;
			
			List<String> idPeliculas= this.colecionIdPeliculasRecomendadas.stream().filter(idDePelicula-> this.apariciones.get(idDePelicula) > mitadDeAmigos && ((unMoovies.usuarioEvaluoLaPeliculaDeId(unUsuario, idDePelicula) == false))).collect(Collectors.toList());
			
			ArrayList<Pelicula> peliculasRecomendadas = unMoovies.getPeliculasConId(idPeliculas);
			this.crearNotificacion(peliculasRecomendadas, unUsuario);

	}

}
