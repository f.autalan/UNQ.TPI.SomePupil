package unq.moovies.sistemaDeRecomendacion;

import java.util.ArrayList;

import java.util.List;
import java.util.stream.Collectors;

import unq.moovies.project.Moovies;

import unq.moovies.project.Pelicula;
import unq.moovies.project.Usuario;
import unq.moovies.sistemaDeCalificacion.Calificacion;

//Precondicion: Moovies en el sistema tiene que tener una ruta de acceso a las calificaciones de IDBM.
//Responsabilidad: Recomendar a un usuario aquellas peliculas 
//				   que fueron evaluadas con 4 o mas y el 
//				   todavia no evaluo
public class RecomendacionPorAmigosEIDBM extends RecomendadorConApariciones  {
	
	//Estructura
	
	//Constructor
	
	public RecomendacionPorAmigosEIDBM() {
		super();
	}
	
	// Mecanismo que recomienda aquellas peliculas que fueron evaluadas por 2 o más amigos con puntaje 3 o superior, ordenadas por el rating de IMDB. 
	@Override
	public void recomendarUsuario(Usuario unUsuario, Moovies unMoovies) {
		List<Usuario> amigosDelUsuario= unUsuario.getColeccionDeAmigos();
		this.agregarCalificacionesDeAmigosAApariciones(unMoovies, amigosDelUsuario, "3");

		
		//Filtro las peliculas recomendadas fijandome que las apariciones sean mayor que 2.
		List<String> idPeliculas= this.colecionIdPeliculasRecomendadas.stream().filter(idDePelicula-> this.apariciones.get(idDePelicula) >= 2 ).collect(Collectors.toList());

		//Me Consigo Las Peliculas Recomendadas
		ArrayList<Pelicula> peliculasRecomendadas = unMoovies.getPeliculasConId(idPeliculas);
		
		
		//Me consigo las Peliculas que estan Calificadas por IDBM
		List<Calificacion> calificacionesDeIDBM = unMoovies.importarCalificaciones();
		List<String> idDePeliculasCalificadasPorIDBM =calificacionesDeIDBM.stream().map(calificacion-> calificacion.getIdPelicula()).collect(Collectors.toList());
		ArrayList<Pelicula> peliculasCalificadasPorIDBM = unMoovies.getPeliculasConId(idDePeliculasCalificadasPorIDBM);
		
		//Hago un Split de las peliculas recomendadas, por un lado pongo las que aparecen calificadas por Idbm, y por otro las que no aparecen calificadas en Idbm.
		
		ArrayList<Pelicula> peliculasRecomendadasYcalificadasPorIDBM = new ArrayList<Pelicula>();
		ArrayList<Pelicula> peliculasRecomendadasYNocalificadasPorIDBM = new ArrayList<Pelicula>();
		
		for (Pelicula unaPeli: peliculasRecomendadas) {
			if (peliculasCalificadasPorIDBM.contains(unaPeli)) {
				peliculasRecomendadasYcalificadasPorIDBM.add(unaPeli);
			}
			else peliculasRecomendadasYNocalificadasPorIDBM.add(unaPeli);
		}
		
		//Ordeno la lista de las peliculas que aparecen calificadas por idbm
		
		peliculasRecomendadasYcalificadasPorIDBM.sort((Pelicula p1,Pelicula p2) -> ((this.getCalificacionCorrespondiente(p1,calificacionesDeIDBM).compareTo(this.getCalificacionCorrespondiente(p2,calificacionesDeIDBM)))));		
		
		
		//A la lista ordenada le agrego al final las que no parecen estar calificadas por idbm
		
		peliculasRecomendadasYcalificadasPorIDBM.addAll(peliculasRecomendadasYNocalificadasPorIDBM);
		
		//Finalmente, al mensaje de recomendacion le agrego el nombre de todas las peliculas validas para recomendar
		this.crearNotificacion(peliculasRecomendadasYcalificadasPorIDBM, unUsuario);

}

	//Proposito: Dada una pelicula y la lista de las calificaciones recibidas desde IDBM, obtiene el rating que IDBM le dio a esa pelicula.
	private Integer getCalificacionCorrespondiente(Pelicula p1, List<Calificacion> calificacionesDeIDBM) {
		Integer resultado = 0;
		for (Calificacion unaCalificacion: calificacionesDeIDBM) {
			if (unaCalificacion.getIdPelicula().equals(p1.getId())) {
				resultado= unaCalificacion.getRating();
			}
		}
		return resultado;
	}

}
