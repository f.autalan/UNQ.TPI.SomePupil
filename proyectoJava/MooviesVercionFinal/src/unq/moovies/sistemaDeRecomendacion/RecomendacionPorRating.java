package unq.moovies.sistemaDeRecomendacion;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import unq.moovies.project.Moovies;
import unq.moovies.project.Pelicula;
import unq.moovies.project.Usuario;
import unq.moovies.sistemaDeCalificacion.Calificacion;

//Responsabilidad:Recomendar un usuario que aquellas pelicuas con mayor
// 				  rating promedio que al menos un amigo haya evaluado
					
public class RecomendacionPorRating extends MetodoDeRecomendacion  {


	//Constructor
	
	public RecomendacionPorRating() {
		super();
	}
	
	//Protocolo
	// Recomendar aquellas películas con mayor rating promedio, que al menos un amigo haya evaluado.
	@Override
	public void recomendarUsuario(Usuario unUsuario, Moovies unMoovies) {
	
		ArrayList<String> idDepeliculasRecomendadas = new ArrayList<String>();
		List<Usuario> amigosDelUsuario= unUsuario.getColeccionDeAmigos();
		
		//Consigo el rating promedio total de las peliculas en Moovies.
		Integer ratingPromedio= unMoovies.getRatingPromedio();
		
		//Consigo las peliculas que tengan mas que ese rating promedio
		ArrayList<Pelicula> peliculasMayoresQueElPromedio= unMoovies.getPeliculasConRatingMayorA(ratingPromedio);
		
		//Consigo todas las pelis evaluadasPorSusAmigos
		for (Usuario unAmigoDelusuario: amigosDelUsuario){
			//Pido la lista de todas las calificaciones que hizo
			ArrayList<Calificacion> calificacionesDelAmigo = unMoovies.getCalificacionesDeUsuario(unAmigoDelusuario);

			//Por cada una de esas calificaciones
			for (Calificacion unaCalificacion: calificacionesDelAmigo) {
				idDepeliculasRecomendadas.add(unaCalificacion.getIdPelicula());
			}
		}
		ArrayList<Pelicula> peliculasRecomendadasPorAmigos = unMoovies.getPeliculasConId(idDepeliculasRecomendadas);
		
		//En las pelis mayores que el promedio, filtro aquellas que los amigos del usuario no evaluo.
		List<Pelicula> peliculasRecomendadas = peliculasMayoresQueElPromedio.stream().filter(unaPelicula -> peliculasRecomendadasPorAmigos.contains(unaPelicula)).collect(Collectors.toList());
		
		this.crearNotificacion(peliculasRecomendadas, unUsuario);
	}

}
