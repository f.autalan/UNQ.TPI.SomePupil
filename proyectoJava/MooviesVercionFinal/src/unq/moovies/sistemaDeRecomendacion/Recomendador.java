package unq.moovies.sistemaDeRecomendacion;

import java.util.ArrayList;

import unq.moovies.project.Moovies;
import unq.moovies.project.Usuario;

//Responsabilidad: Permitir al usuario elegir como  le gustaria
//				   que moovies le recomiende peliculas y elegir
//				   entre diferentes metodos a gusto,		
public class Recomendador {

	//Estructura
	private ArrayList<MetodoDeRecomendacion> recomendaciones;
	private MetodoDeRecomendacion recomendacionActual;
	
	//Constructores
	
	public Recomendador() {
		super();
		this.recomendaciones = new ArrayList<MetodoDeRecomendacion>();
	}
	
	//setters
	public void setRecomendacionActual(MetodoDeRecomendacion unaRecomendacion){
		this.recomendacionActual=unaRecomendacion;
	}
	//Protocolo
	
	public void recomendarUsuario(Usuario unUsuario, Moovies unMoovies){
		this.recomendacionActual.recomendarUsuario(unUsuario,unMoovies);
	}
	
	public void agregarRecomendacion(MetodoDeRecomendacion unaRecomendacion) {
		this.recomendaciones.add(unaRecomendacion);	
	}
	
	//Proposito: permite al usuario cambiar el metodo de recomendacion actual.
	//Precondicion: En recomendaciones tiene que existir la recomendacion buscada en tal index.
	public void cambiarMetodoDeRecomendacion(int unIndex) {
		this.setRecomendacionActual(this.recomendaciones.get(unIndex));
	}
		
}
