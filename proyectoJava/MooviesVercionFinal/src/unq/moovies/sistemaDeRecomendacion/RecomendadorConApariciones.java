package unq.moovies.sistemaDeRecomendacion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import unq.moovies.project.Moovies;
import unq.moovies.project.Usuario;
import unq.moovies.sistemaDeBusqueda.EsMayorIgual;
import unq.moovies.sistemaDeBusqueda.FiltradoSimple;
import unq.moovies.sistemaDeBusqueda.Rating;
import unq.moovies.sistemaDeCalificacion.Calificacion;

//Responsabilidad:Recomendar por apariciones de calificaciones en amigos.
public abstract class RecomendadorConApariciones extends MetodoDeRecomendacion {
	
	protected HashMap<String,Integer> 	apariciones;
	protected List<String> 				colecionIdPeliculasRecomendadas;
	protected List<Usuario>				colecionDeCalificaciones;
	
	public RecomendadorConApariciones(){
		this.setApariciones(new HashMap<String,Integer>());
		this.setColecionIdPeliculasRecomendadas(new ArrayList<String>());
		this.setColecionDeAmigos(new ArrayList<Usuario>());
	}

	//Setters
	public void setColecionIdPeliculasRecomendadas(List<String> colecionIdPeliculasRecomendadas) {
		this.colecionIdPeliculasRecomendadas = colecionIdPeliculasRecomendadas;
	}

	public void setApariciones(HashMap<String,Integer> apariciones) {
		this.apariciones = apariciones;
	}

	public void setColecionDeAmigos(List<Usuario> colecionDeAmigos) {
		this.colecionDeCalificaciones = colecionDeAmigos;
	}
	//Protocolo
	public void agregarCalificacionesDeAmigosAApariciones(Moovies unMoovies,List<Usuario> amigosDelUsuario,String unValor){
		ArrayList<Calificacion> calificacionesDeAmigos=unMoovies.getCalificacionesDeUsuarios(amigosDelUsuario);

		Rating 	 			raiting				= new Rating();
		EsMayorIgual	 	mayorIgual			= new EsMayorIgual();
		FiltradoSimple 		filtroSimple		= new FiltradoSimple(raiting,unValor,mayorIgual);
		
		
		List<Calificacion> coleccionFiltrada	=  filtroSimple.ejecutarFiltrado(calificacionesDeAmigos);

		for(Calificacion unaCalificacion: coleccionFiltrada){
			this.agregarApariciones(unaCalificacion);
			
		}
		
	}

	private void agregarApariciones(Calificacion unaCalificacion) {
		if (this.apariciones.containsKey(unaCalificacion.getIdPelicula())){
			this.apariciones.put(unaCalificacion.getIdPelicula(), this.apariciones.get(unaCalificacion.getIdPelicula())+1);
		}

		else {
			this.apariciones.put(unaCalificacion.getIdPelicula(), 1);
			this.colecionIdPeliculasRecomendadas.add(unaCalificacion.getIdPelicula());
		}
		
	}
}
