package unq.moovies.sistemaInicioDeSesion;


import java.util.HashMap;
import java.util.List;

import unq.moovies.project.Usuario;
import unq.moovies.sistemaDeAdministradores.Administrador;

//Responsabilidad:Se encarga de guardar todos los identifiaciones de los usuarios que
//				  se registraron en el sistema.
public class AdministradorDeInicioDeSesion extends Administrador<Identificacion>{

	
	private HashMap<String,Identificacion> diccionario;
	
	
	//Contructor	
	public AdministradorDeInicioDeSesion(){
		this.diccionario=new HashMap<String, Identificacion>();
		
				
	}
	
	
	
	//Protocolo
	
	//Prosito:Agrega una k<key> y v<valor> al map
	//Precondiccion:-
	@Override
	public
	void agregarElemento(Identificacion identificador) {
		this.diccionario.put(identificador.getNick(), identificador);
		
	}

	 //Proposito:Denota true en caso que la key del inditeficador se encuentra en el map
	//y su valor es igual al valor del identificador del map
	//Caso contrario denota false
	//Precondicion:-
		
	
	public Boolean tieneRegistradoAElemento(Identificacion identificador) {
		String nick=identificador.getNick();//variable estetica
		String contrasenia=identificador.getContrasenia();//variable estetica
		Identificacion identi=diccionario.get(nick);//variable estetica
		boolean respuesta=false;
		if(identi!=null){
			boolean condicion1=diccionario.containsKey(nick);//variable estetica
			boolean condicion2=identi.getContrasenia()==contrasenia;//variable estetica
			respuesta= condicion1 && condicion2;
		}
		
		return respuesta;
	}
	
	//Prosito:Agrega los indentifacadores al map
	//Precondiccion:-
	public void agregarUsuariosRegistrados(List<Usuario> usuarios){
		
		for(Usuario usuario: usuarios){
		
			this.agregarElemento(usuario.identificar());
		}
		
	
	}
	



	
	
}
