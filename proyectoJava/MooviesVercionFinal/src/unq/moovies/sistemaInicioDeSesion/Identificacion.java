package unq.moovies.sistemaInicioDeSesion;

//Resposabilidad: Se encarga de guardar el nick y pass introducido por un usuario.
public class Identificacion {
	//Contrucctor
	private String nick;
	private String contrasenia;
	
	public Identificacion(String unNick,String unaContrasenia){
		this.setNick(unNick);
		this.setContrasenia(unaContrasenia);	
	}
	
	
	
	//Getters
	public String getNick() {
		return nick;
	}
	
	public String getContrasenia() {
		return contrasenia;
	}
	
	//Setters
	public void setContrasenia(String v) {
		this.contrasenia = v;
	}
	
	public void setNick(String k) {
		this.nick = k;
	}
	
	
	
}
