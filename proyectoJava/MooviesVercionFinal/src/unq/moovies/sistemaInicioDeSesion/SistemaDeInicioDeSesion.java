package unq.moovies.sistemaInicioDeSesion;

import java.util.ArrayList;
import java.util.List;

import unq.moovies.project.Usuario;

//Responsabilidad: Se encarga de logear y desconectar a los usuarios al sistema moovis.
public class SistemaDeInicioDeSesion {
	
	private ArrayList<String> conectados;
	private AdministradorDeInicioDeSesion administrador;
	
	//Contructor
	public SistemaDeInicioDeSesion(){
		
		this.setLogeados(new ArrayList<String>());
		this.setManager(new AdministradorDeInicioDeSesion());
		
	
	}
	
	
	
	
	//Setters
	public void setLogeados(ArrayList<String> logeados) {
		this.conectados = logeados;
	}

	
	public void setManager(AdministradorDeInicioDeSesion manager) {
		this.administrador = manager;
	}

	
	
	//Protocolo
	
	//Proposito:Agrega un nick<unNick> al listado de logeados si este esa registrado en el manager.
	//Precondicion:unNick y unaContrasenia deben ser tipado como un String.
	public void logear(Identificacion identificacion){
		
		if(administrador.tieneRegistradoAElemento(identificacion)){
			
			this.conectados.add(identificacion.getNick());
		}
		
	}


	//Proposito:Denota true si un nick<unNick> pertece a listado de conectados.
	//Precondicion:-
	public boolean seEncuentraConectado(String unNick) {
		boolean respuesta=false;
		for (String nombre:this.conectados){
			   if(nombre.equals(unNick)){
				   respuesta=true;
		 
			   }
		}
		return respuesta;
		
		
	}


	//Proposito:Devuelve la cantidad de  conectados.
	//Precondicion:-
	public Integer cantConectados() {
		
		return this.conectados.size();
	}


	//Proposito:Elimina un nick<unNick> de la lista de conectados.
	//Precondicion:-
	public void desconectar(String unNick) {
		
		this.conectados.remove(unNick);
	}


	//Propsito:Integra un listado de usuarios a Administrador.
	public void integrarUsuariosRegistrados(List<Usuario> usuarios) {
		this.administrador.agregarUsuariosRegistrados(usuarios);
		
	}
		

}
