package unq.moovies.Genero;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import unq.moovies.Genero.Genero;
import unq.moovies.Genero.GeneroGenerico;
import unq.moovies.Genero.GeneroEspecifico;
import unq.moovies.project.Notificacion;

import unq.moovies.project.Usuario;

public class TestGeneroEspecifico {

	private GeneroEspecifico 					hombresLobosGeneroEspecifico;
	private GeneroEspecifico 					acechadoresNocturnosGeneroEspecifico;
	private GeneroGenerico						terrorGeneroGenerico;
	private List<Genero> 						spyColectionGenero;
	private Notificacion						mockNotificacion;
	private Usuario								mockFernando;
	
	@Before
	public void setUp() throws Exception {
	
		this.mockFernando								= mock(Usuario.class);
		this.spyColectionGenero							= spy( new ArrayList<Genero>());
		this.terrorGeneroGenerico						= mock(GeneroGenerico.class);
		this.mockNotificacion							= mock(Notificacion.class);
		this.acechadoresNocturnosGeneroEspecifico		= new GeneroEspecifico("AsechadoresNocturnos");	
		this.hombresLobosGeneroEspecifico				= spy(new GeneroEspecifico("HombresLobos"));
		
		List<Genero> paraRespuestaDeMock 				= new ArrayList<Genero>();
		paraRespuestaDeMock.add(this.terrorGeneroGenerico);
		when(this.terrorGeneroGenerico.compactar()).thenReturn(paraRespuestaDeMock);
		
	}

	@Test
	public void test0000AunGeneroEspecificoSeLeAgregaUnGeneros(){
		this.hombresLobosGeneroEspecifico.setColeccionDeGenero(this.spyColectionGenero);
		this.hombresLobosGeneroEspecifico.agregar(this.acechadoresNocturnosGeneroEspecifico);
		verify(this.spyColectionGenero).add(this.acechadoresNocturnosGeneroEspecifico);
		
		assertEquals(1,this.hombresLobosGeneroEspecifico.getColeccionDeGenero().size());
			
	}
	@Test
	public void test0001AUnGeneroEspecificoSeLePuedeBorrarUnGenero(){
		this.acechadoresNocturnosGeneroEspecifico.setColeccionDeGenero(this.spyColectionGenero);
		this.acechadoresNocturnosGeneroEspecifico.agregar(this.terrorGeneroGenerico);
		this.acechadoresNocturnosGeneroEspecifico.eliminar(this.terrorGeneroGenerico);
		verify(this.spyColectionGenero).remove(this.terrorGeneroGenerico);
		assertEquals(0,this.acechadoresNocturnosGeneroEspecifico.getColeccionDeGenero().size());
	}
	
	@Test
	public void test0002UnGeneroEspecificoPuedeCompactarTodosSusGeneros(){
		this.hombresLobosGeneroEspecifico.agregar(this.acechadoresNocturnosGeneroEspecifico);
		this.acechadoresNocturnosGeneroEspecifico.agregar(this.terrorGeneroGenerico);
		List<Genero> resultadoEsperado = this.hombresLobosGeneroEspecifico.compactar();
		verify(this.hombresLobosGeneroEspecifico).compactar();
	
		
		assertTrue(resultadoEsperado.contains(this.hombresLobosGeneroEspecifico));
		assertTrue(resultadoEsperado.contains(this.acechadoresNocturnosGeneroEspecifico));
		assertTrue(resultadoEsperado.contains(this.terrorGeneroGenerico));
		
		
	}
	@Test
	public void test0003ElNombreDeLosGenerosEspecificosSonHombreLoboyAsechadoresNocturnos(){
		
		assertEquals("HombresLobos",this.hombresLobosGeneroEspecifico.getNombreGenero());
		assertEquals("AsechadoresNocturnos",this.acechadoresNocturnosGeneroEspecifico.getNombreGenero());
	}
	
	@Test
	public void test0004AlGeneroEspecificoHombreLoboSeSuscribeElUsuarioFernando(){
		this.hombresLobosGeneroEspecifico.subscribirse(this.mockFernando);
		
		verify(this.hombresLobosGeneroEspecifico).subscribirse(this.mockFernando);
		
		assertEquals(1,this.hombresLobosGeneroEspecifico.getColeccionDeUsuario().size());
	}
	
	@Test
	public void test00005AUnGeneroEspecificoSePuedeNotificarASusUsuariosYNotificarLosUsuariosDeSusGenerosMasGenericosUnaPelicula(){
		this.hombresLobosGeneroEspecifico.subscribirse(this.mockFernando);
		this.hombresLobosGeneroEspecifico.agregar(this.acechadoresNocturnosGeneroEspecifico);
	
		this.acechadoresNocturnosGeneroEspecifico.agregar(this.terrorGeneroGenerico);
		
		this.hombresLobosGeneroEspecifico.notificar(this.mockNotificacion);
		verify(this.hombresLobosGeneroEspecifico).notificar(this.mockNotificacion);
	}
	
	

}
