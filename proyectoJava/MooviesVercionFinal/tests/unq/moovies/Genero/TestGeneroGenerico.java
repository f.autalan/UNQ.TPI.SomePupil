package unq.moovies.Genero;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import unq.moovies.Genero.GeneroGenerico;
import unq.moovies.project.Notificacion;

import unq.moovies.project.Usuario;

public class TestGeneroGenerico {
	
	private GeneroGenerico 		terrorGeneroGenerico;
	private Usuario 			mockFernando;
	private Usuario 			mockIvan;
	private Notificacion 		mockNotifiacion;
	
	@Before
	public void setUp() throws Exception {
		this.terrorGeneroGenerico	=spy(new GeneroGenerico("Terror"));
		
		this.mockFernando			=mock(Usuario.class);
		this.mockIvan				=mock(Usuario.class);
		this.mockNotifiacion		=mock(Notificacion.class);
		
		
	}

	@Test
	public void test0000AlGeneroGenericoTerrorSeSuscribeUnUsuario() {
		this.terrorGeneroGenerico.subscribirse(this.mockFernando);
		
		verify(this.terrorGeneroGenerico).subscribirse(this.mockFernando);
		
		assertEquals(1,this.terrorGeneroGenerico.getColeccionDeUsuario().size());
	}
	@Test
	public void test0001AlGeneroGenericoTerrorSeDesubscribeUnUsuario() {
		this.terrorGeneroGenerico.subscribirse(this.mockIvan);
		this.terrorGeneroGenerico.desubcribirse(this.mockIvan);
		verify(this.terrorGeneroGenerico).desubcribirse(this.mockIvan);
		assertEquals(0,this.terrorGeneroGenerico.getColeccionDeUsuario().size());
	}
	
	@Test
	public void tes00003tElGeneroGenericoTerrorNotificaALosUsuariosSuscriptoLaNuevaPeliculaPertenecienteASuGenero() {
		this.terrorGeneroGenerico.subscribirse(this.mockIvan);
		
		this.terrorGeneroGenerico.notificar(this.mockNotifiacion);
		verify(this.terrorGeneroGenerico).notificar(this.mockNotifiacion);
		
	}
	@Test
	public void test0004ElGeneroGenericoTerrorPuedeCompactarse() {
		List<Genero> comparacion=new ArrayList<Genero>();
		comparacion.add(this.terrorGeneroGenerico);
		List<Genero> generoCompactado =this.terrorGeneroGenerico.compactar();
		
		assertEquals(comparacion,generoCompactado);
		
	}
	@Test
	public void test0005ElNombreDelGeneroEs(){
		assertEquals("Terror",this.terrorGeneroGenerico.getNombreGenero());
	}
	
	
	
}
