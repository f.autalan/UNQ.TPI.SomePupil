package unq.moovies.Genero;



import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import unq.moovies.project.Pelicula;
import unq.moovies.project.PublicInfo;
import unq.moovies.project.Reproduccion;

public class TestPelicula {

	private Pelicula 		peliculaTest;
	private PublicInfo 		mockPublicInfo;
	private Genero 			mockGenero;
	private Reproduccion 	mockReproduccion;
	
	@Before
	public void setUp() throws Exception {
		this.mockPublicInfo		=mock(PublicInfo.class);
		this.mockGenero			=mock(Genero.class);
		this.mockReproduccion	=mock(Reproduccion.class);
		this.peliculaTest		=spy(new Pelicula(this.mockPublicInfo,this.mockGenero,this.mockReproduccion));
	}
	
	@Test
	public void testUnaPeliculaPuedeNotificarASuGneero() {
		this.peliculaTest.notificarExtreno();
		verify(this.peliculaTest).notificarExtreno();	
	}

}
