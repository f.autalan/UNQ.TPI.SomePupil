package unq.moovies.Genero;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;

import unq.moovies.project.Notificacion;
import unq.moovies.project.ProfileUsuario;
import unq.moovies.project.Usuario;

public class TestUsuarioNotifiacion {

	private Usuario 			unUsuarioTes;
	private ProfileUsuario 		mockProfileUsuario;
	private Notificacion 		mockNotificacion;
	
	@Before
	public void setUp() throws Exception {
	
		this.mockProfileUsuario		=mock(ProfileUsuario.class);
		this.mockNotificacion		=mock(Notificacion.class);
		this.unUsuarioTes			=spy(new Usuario("1","2",this.mockProfileUsuario,"3"));
		
	}

	@Test
	public void testSeAgregoUnExtrenoALaListaDeEstrenosDelUsuario() {
		this.unUsuarioTes.agregarNotificacion(this.mockNotificacion);
		verify(this.unUsuarioTes).agregarNotificacion(this.mockNotificacion);
		assertEquals(1,this.unUsuarioTes.getColeccionDeNotificaciones().size());
	}
	
	@Test
	public void testSeBorroUnExtrenoALaListaDeEstrenosDelUsuario() {
		this.unUsuarioTes.agregarNotificacion(this.mockNotificacion);
		this.unUsuarioTes.borrarNotificacion(this.mockNotificacion);
		verify(this.unUsuarioTes).borrarNotificacion(this.mockNotificacion);
		assertEquals(0,this.unUsuarioTes.getColeccionDeNotificaciones().size());
	}

}
