package unq.moovies.SistemaDeInicioDeSesion;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

//import static org.mockito.Mockito.when;



import org.junit.Before;
import org.junit.Test;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import unq.moovies.project.Usuario;
import unq.moovies.sistemaInicioDeSesion.AdministradorDeInicioDeSesion;
import unq.moovies.sistemaInicioDeSesion.Identificacion;




public class TestAdministradorDeInicioDeSesion {
	
	private AdministradorDeInicioDeSesion adminInicioDeSesion;
	private List<Usuario> usuarios;
	@Mock Identificacion mockIdentificacionIvan;
	@Mock Identificacion mockIdentificacionFernando;
	@Mock Usuario mockFernandoUser;
	@Mock Usuario mockIvanUser;
	
	@Before
	public void setUp() throws Exception {	
		this.adminInicioDeSesion= new AdministradorDeInicioDeSesion();
		this.usuarios=new ArrayList<Usuario>();
		MockitoAnnotations.initMocks(this);
		when(mockIdentificacionFernando.getNick()).thenReturn("Fernando");
		when(mockIdentificacionFernando.getContrasenia()).thenReturn("123456");
		when(mockFernandoUser.identificar()).thenReturn(mockIdentificacionFernando);
		when(mockIdentificacionIvan.getNick()).thenReturn("Ivan");
		when(mockIdentificacionIvan.getContrasenia()).thenReturn("654321");
		when(mockIvanUser.identificar()).thenReturn(mockIdentificacionIvan);
		this.usuarios.add(mockFernandoUser);
		this.usuarios.add(mockIvanUser);
	}
	
	@Test
	public void test0000ElAdministradorDeInicioDeSesionRegistroAlUsuarioFernando() {
		this.adminInicioDeSesion.agregarElemento(mockIdentificacionFernando);
		
		assertTrue(this.adminInicioDeSesion.tieneRegistradoAElemento(mockIdentificacionFernando));
	}
	@Test
	public void testEl0001AdministradorDeInicioDeSesionRegistroAlUsuarioFernando() {
		
		this.adminInicioDeSesion.agregarUsuariosRegistrados(this.usuarios);
		
		assertTrue(this.adminInicioDeSesion.tieneRegistradoAElemento(mockIdentificacionFernando));
	}
	
	
}
