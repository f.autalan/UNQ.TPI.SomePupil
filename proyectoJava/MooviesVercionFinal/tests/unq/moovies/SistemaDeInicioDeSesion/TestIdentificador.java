package unq.moovies.SistemaDeInicioDeSesion;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import unq.moovies.sistemaInicioDeSesion.Identificacion;

public class TestIdentificador {
	
	private Identificacion identifiacionTest;
	@Before
	public void setUp() throws Exception {
		this.identifiacionTest		= new Identificacion("Juan","123456");
	}

	@Test
	public void testLaClaveDeLaIdentificacionEs123456YElNickEsJuan() {
		assertEquals("Juan",this.identifiacionTest.getNick());
		assertEquals("123456",this.identifiacionTest.getContrasenia());
	}

}
