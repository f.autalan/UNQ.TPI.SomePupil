package unq.moovies.SistemaDeInicioDeSesion;

import static org.junit.Assert.*;


import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

import unq.moovies.project.Usuario;
import unq.moovies.sistemaInicioDeSesion.Identificacion;
import unq.moovies.sistemaInicioDeSesion.SistemaDeInicioDeSesion;

public class TestSistemaDeInicioSesionMock {
	//Estructura:
	private SistemaDeInicioDeSesion iniciarSesion;
	private List<Usuario> usuarios;
	@Mock Usuario mockFernandoUser;
	@Mock Usuario mockIvanUser;
	@Mock Identificacion mockIdentifiacionDelUserFernando;
	@Mock Identificacion mockIdentifiacionDelUserFernandoFormaIncorrecta;
	@Mock Identificacion mockIdentifiacionDelUserIvan;
	
	@Before
	public void setUp() throws Exception {
		this.iniciarSesion=new SistemaDeInicioDeSesion();
		this.usuarios=new ArrayList<Usuario>();
		MockitoAnnotations.initMocks(this);
		when(mockIdentifiacionDelUserFernando.getNick()).thenReturn("Fernando");
		when(mockIdentifiacionDelUserFernando.getContrasenia()).thenReturn("123456");
		when(mockIdentifiacionDelUserFernandoFormaIncorrecta.getNick()).thenReturn("Fernando");
		when(mockIdentifiacionDelUserFernandoFormaIncorrecta.getContrasenia()).thenReturn("32145");
		when(mockFernandoUser.identificar()).thenReturn(mockIdentifiacionDelUserFernando);
		when(mockIdentifiacionDelUserIvan.getNick()).thenReturn("Ivan");
		when(mockIdentifiacionDelUserIvan.getContrasenia()).thenReturn("654321");
		when(mockIvanUser.identificar()).thenReturn(mockIdentifiacionDelUserIvan);
		this.usuarios.add(mockFernandoUser);
		this.usuarios.add(mockIvanUser);
		this.iniciarSesion.integrarUsuariosRegistrados(this.usuarios);
		
		
	}
	@Test
	public void test0001EnElSistemaSeEncuentraConectadoElUsuarioFernando() {
		//ejecucion
		iniciarSesion.logear(mockIdentifiacionDelUserFernando);
	
		//asercion
		
		assertTrue(iniciarSesion.seEncuentraConectado("Fernando"));
	}
	
	@Test
	public void test0002EnElSistemaNoSeEncuetraConectadoElUsuarioFernando(){
		//ejecucion
		iniciarSesion.logear(mockIdentifiacionDelUserFernandoFormaIncorrecta);
		//asercion
		assertFalse(iniciarSesion.seEncuentraConectado("Fernando"));
	}
	
	@Test
	public void test0003EnElSistemaHayConectado2UsuariosActua(){
		//ejecucion
		iniciarSesion.logear(mockIdentifiacionDelUserFernando);
		iniciarSesion.logear(mockIdentifiacionDelUserIvan);	
		//asercion
		assertTrue(iniciarSesion.cantConectados().equals(2));
		
	}
	@Test
	public void test0004ElSistemaDespuesQueSeDesconecteElUsuarioIvanTiene1UsuarioConectado(){
		//ejecucion
		iniciarSesion.logear(mockIdentifiacionDelUserFernando);
		iniciarSesion.logear(mockIdentifiacionDelUserIvan);	
		iniciarSesion.desconectar("Ivan");
		//asercion
		assertTrue(iniciarSesion.cantConectados().equals(1));
	}
	@Test
	public void test0005DelSistemaSeDesconectoElUsuarioFernando(){
		//ejecucion
		iniciarSesion.logear(mockIdentifiacionDelUserFernando);
		iniciarSesion.desconectar("Fernando");
		
		//asercion
		assertFalse(iniciarSesion.seEncuentraConectado("Fernando"));
	}
	

}
