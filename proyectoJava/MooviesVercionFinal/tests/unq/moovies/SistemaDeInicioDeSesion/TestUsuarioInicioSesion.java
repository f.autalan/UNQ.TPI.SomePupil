package unq.moovies.SistemaDeInicioDeSesion;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import unq.moovies.project.Moovies;
import unq.moovies.project.ProfileUsuario;
import unq.moovies.project.Usuario;

public class TestUsuarioInicioSesion {

	private Usuario 			unUsuarioTest;
	private ProfileUsuario 		unProfileUsuarioMock;
	private Moovies				unMockMovies;
	@Before
	public void setUp() throws Exception {
		this.unMockMovies				= mock(Moovies.class);
		this.unProfileUsuarioMock		= mock(ProfileUsuario.class);
		this.unUsuarioTest				= spy(new Usuario("Hunter","321",this.unProfileUsuarioMock,"1"));
	}

	@Test
	public void test0000UnUsuarioSeConectaAlSistemaYLuegoSeDesconecta() {
		this.unUsuarioTest.iniciarSesion(this.unMockMovies);
		verify(this.unUsuarioTest).iniciarSesion(this.unMockMovies);
		
		this.unUsuarioTest.desconectarse(this.unMockMovies);
		verify(this.unUsuarioTest).desconectarse(this.unMockMovies);
		
	}
	@Test
	public void test0001UnUsuarioTieneUnaIdentificacion() {
		assertTrue(this.unUsuarioTest.identificar().getNick().equals("Hunter"));
		assertTrue(this.unUsuarioTest.identificar().getContrasenia().equals("321"));
		
	}
	

}
