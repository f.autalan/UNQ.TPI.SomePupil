package unq.moovies.SistemaDeRecomendacion;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;

import unq.moovies.Genero.Genero;
import unq.moovies.project.Moovies;
import unq.moovies.project.Pelicula;
import unq.moovies.project.ProfileUsuario;
import unq.moovies.project.PublicInfo;
import unq.moovies.project.Reproduccion;
import unq.moovies.project.Usuario;
import unq.moovies.sistemaDeRecomendacion.RecomendacionPorAmigos;
import unq.moovies.sistemaDeRecomendacion.RecomendacionPorAmigosEIDBM;
import unq.moovies.sistemaDeRecomendacion.RecomendacionPorRating;

public class SistemaDeRecomendacionTest {

	// Instancias que se utilizan en el test.
	private Moovies moovies;
	private RecomendacionPorAmigos recomendacionPorAmigos;
	private RecomendacionPorAmigosEIDBM recomendacionPorIbdm;
	private RecomendacionPorRating recomendacionPorRating;

	private Usuario pepita;
	private Usuario gandalf;
	private Usuario trump;
	private Usuario mrT;
	private Usuario odin;
	private Usuario jackeldestripador;
	
	private Pelicula durodematar;
	private Pelicula historiasinfin;
	private Pelicula rushhour;
	private Pelicula pokemon;
	private Pelicula terminator;
	private Genero	 mockGenero;
	
	
	//Setup del test, se In
	

	@Before

	public void setup(){
		//SetupDelSistema
		moovies 				= spy(new Moovies());
		recomendacionPorAmigos 	= new RecomendacionPorAmigos();
		recomendacionPorRating 	= new RecomendacionPorRating();
		recomendacionPorIbdm 	= new RecomendacionPorAmigosEIDBM();
		moovies.setRutaCalificaciones("/home/fernando/data/u.data.new.csv");
		this.mockGenero			= mock(Genero.class);
		
		//SetupDeLosUsuarios

		pepita= new Usuario("pepita","123", (new ProfileUsuario("pepita","golondrina", "1", "123", "mensajera")),"1");
		gandalf= new Usuario("gandalf","222", (new ProfileUsuario("gandalf","elgris", "200", "143", "mago")),"2");
		trump= new Usuario("trump","333", (new ProfileUsuario("trump","elpresi", "70", "143", "presidente")),"3");
		mrT= new Usuario("mrT","223", (new ProfileUsuario("mrT","brigada", "200", "143", "mercenario")),"4");
		odin= new Usuario("odin","122", (new ProfileUsuario("odin","nordico", "200", "143", "dios")),"5");
		jackeldestripador= new Usuario("jackeldestripador","222", (new ProfileUsuario("jackeldestripador","elgris", "200", "143", "asesino")),"6");
		
		//SetupDeLasPeliculas
		durodematar			= new Pelicula (new PublicInfo("durodematar","unAnioDeEstreno","1", (new ArrayList<String>())),this.mockGenero,new Reproduccion(50));
		historiasinfin		= new Pelicula(new PublicInfo("historiasinfin","unAnioDeEstreno","2", (new ArrayList<String>())),this.mockGenero,new Reproduccion(50));
	    rushhour			= new Pelicula(new PublicInfo("rushhour","unAnioDeEstreno","3", (new ArrayList<String>())),this.mockGenero,new Reproduccion(50));
		pokemon				= new Pelicula(new PublicInfo("pokemon","unAnioDeEstreno","4", (new ArrayList<String>())),this.mockGenero,new Reproduccion(50));
		terminator			= new Pelicula(new PublicInfo("terminator","unAnioDeEstreno","5", (new ArrayList<String>())),this.mockGenero,new Reproduccion(50));
		
		//Moovies integra a los usuarios y peliculas
		
		moovies.agregarPelicula(durodematar);
		moovies.agregarPelicula(historiasinfin);
		moovies.agregarPelicula(rushhour);
		moovies.agregarPelicula(pokemon);
		moovies.agregarPelicula(terminator);
		moovies.agregarUsuario(pepita);
		moovies.agregarUsuario(gandalf);
		moovies.agregarUsuario(trump);
		moovies.agregarUsuario(mrT);
		moovies.agregarUsuario(odin);
		moovies.agregarUsuario(jackeldestripador);
		
		//Pepita se hace amiga del resto de los usuarios
		 
		pepita.establecerAmistad(gandalf);
		pepita.establecerAmistad(trump);
		pepita.establecerAmistad(mrT);
		pepita.establecerAmistad(odin);
		pepita.establecerAmistad(jackeldestripador);

		
		pepita.agregarRecomendacion(recomendacionPorAmigos);
		pepita.agregarRecomendacion(recomendacionPorRating);
		pepita.agregarRecomendacion(recomendacionPorIbdm);
	}
	
	
	@Test
	public void test00CuandoSeRecomiendaPorAmigosAUnUsuarioSeObtieneLasPeliculasQueMasDeLaMitadDeSusAmigosEvaluaronCon4oMasYElTodaviaNoEvaluo() {
		//SetUp: Se guarda las reproducciones individuales que reciben los usuarios cuando le indican a moovies que quieren ver una pelicula
		Reproduccion reproduccionDeDuroDeMatarDeGandalf = moovies.verPelicula(durodematar);
		Reproduccion reproduccionDeDuroDeMatarDeTrump = moovies.verPelicula(durodematar);
		Reproduccion reproduccionDeDuroDeMatarDeMrT = moovies.verPelicula(durodematar);
		Reproduccion reproduccionDeDuroDeMatarDeOdin = moovies.verPelicula(durodematar);
		
		moovies.darlePlayAReproduccion(reproduccionDeDuroDeMatarDeMrT, mrT);
		moovies.darlePlayAReproduccion(reproduccionDeDuroDeMatarDeTrump, trump);
		moovies.darlePlayAReproduccion(reproduccionDeDuroDeMatarDeGandalf, gandalf);
		moovies.darlePlayAReproduccion(reproduccionDeDuroDeMatarDeOdin, odin);
		
		moovies.calificarPelicula(mrT, durodematar,4,"Bruce willis es un pescao");
		moovies.calificarPelicula(odin, durodematar,4,"Bruce willis es un pescao");
		moovies.calificarPelicula(trump, durodematar,4,"Bruce willis es un pescao");
		moovies.calificarPelicula(gandalf, durodematar,1,"Bruce willis es un pescao");
		
		pepita.setRecomendacion(0);
		
		String textoDeRecomendacion = "Moovies aprecia tu subscripcion y conociendote como nadie mas, esta temporada te recomienda: durodematar, ";

		//assertTrue(moovies.recomendarUsuario(pepita).equals(textoDeRecomendacion));
		moovies.recomendarUsuario(pepita);
		verify(moovies).recomendarUsuario(pepita);
		assertTrue(pepita.getColeccionDeNotificaciones().stream().anyMatch(notificacion-> notificacion.getMensaje().equals(textoDeRecomendacion)));
		
	}
	

	@Test
	public void test01CuandoSeRecomiendaPorRatingAUnUsuarioSeObtieneLasPeliculasQueAlMenosUnAmigoCalificoYTieneMasCAlificacionQueElRatingPromedio() {
		//SetUp: Se guarda las reproducciones individuales que reciben los usuarios cuando le indican a moovies que quieren ver una pelicula
		Reproduccion reproduccionDeDuroDeMatarDeGandalf = moovies.verPelicula(durodematar);
		Reproduccion reproduccionDeDuroDeMatarDeTrump = moovies.verPelicula(durodematar);
		Reproduccion reproduccionDeDuroDeMatarDeMrT = moovies.verPelicula(durodematar);
		Reproduccion reproduccionDeDuroDeMatarDeOdin = moovies.verPelicula(durodematar);
				
		Reproduccion reproduccionDePokemonDeGandalf = moovies.verPelicula(pokemon);
		Reproduccion reproduccionDePokemonDeTrump = moovies.verPelicula(pokemon);
		Reproduccion reproduccionDePokemonDeMrT = moovies.verPelicula(pokemon);
		Reproduccion reproduccionDePokemonDeOdin = moovies.verPelicula(pokemon);
		
		Reproduccion reproduccionDeTerminatorDeGandalf = moovies.verPelicula(terminator);
		Reproduccion reproduccionDeTerminatorDeTrump = moovies.verPelicula(terminator);
		Reproduccion reproduccionDeTerminatorDeMrT = moovies.verPelicula(terminator);
		Reproduccion reproduccionDeTerminatorDeOdin = moovies.verPelicula(terminator);
		
		moovies.darlePlayAReproduccion(reproduccionDeDuroDeMatarDeMrT, mrT);
		moovies.darlePlayAReproduccion(reproduccionDeDuroDeMatarDeTrump, trump);
		moovies.darlePlayAReproduccion(reproduccionDeDuroDeMatarDeGandalf, gandalf);
		moovies.darlePlayAReproduccion(reproduccionDeDuroDeMatarDeOdin, odin);
		
		moovies.darlePlayAReproduccion(reproduccionDePokemonDeMrT, mrT);
		moovies.darlePlayAReproduccion(reproduccionDePokemonDeTrump, trump);
		moovies.darlePlayAReproduccion(reproduccionDePokemonDeGandalf, gandalf);
		moovies.darlePlayAReproduccion(reproduccionDePokemonDeOdin, odin);
		
		moovies.darlePlayAReproduccion(reproduccionDeTerminatorDeMrT, mrT);
		moovies.darlePlayAReproduccion(reproduccionDeTerminatorDeTrump, trump);
		moovies.darlePlayAReproduccion(reproduccionDeTerminatorDeGandalf, gandalf);
		moovies.darlePlayAReproduccion(reproduccionDeTerminatorDeOdin, odin);
		
		moovies.calificarPelicula(mrT, durodematar,1,"Bruce willis es un pescao");
		moovies.calificarPelicula(odin, durodematar,2,"Bruce willis es un pescao");
		moovies.calificarPelicula(trump, durodematar,2,"Bruce willis es un pescao");
		moovies.calificarPelicula(gandalf, durodematar,1,"Bruce willis es un pescao");
		
		moovies.calificarPelicula(mrT, pokemon,4,"Bruce willis es un pescao");
		moovies.calificarPelicula(odin, pokemon,4,"Bruce willis es un pescao");
		moovies.calificarPelicula(trump, pokemon,4,"Bruce willis es un pescao");
		moovies.calificarPelicula(gandalf, pokemon,5,"Bruce willis es un pescao");
		
		moovies.calificarPelicula(mrT, terminator,3,"Bruce willis es un pescao");
		moovies.calificarPelicula(odin, terminator,5,"Bruce willis es un pescao");
		moovies.calificarPelicula(trump, terminator,4,"Bruce willis es un pescao");
		moovies.calificarPelicula(gandalf, terminator,5,"Bruce willis es un pescao");
		
		pepita.setRecomendacion(1);
		
		String textoDeRecomendacion = "Moovies aprecia tu subscripcion y conociendote como nadie mas, esta temporada te recomienda: pokemon, terminator, ";
		//assertTrue(moovies.recomendarUsuario(pepita).equals(textoDeRecomendacion));
		moovies.recomendarUsuario(pepita);
		verify(moovies).recomendarUsuario(pepita);
		assertTrue(pepita.getColeccionDeNotificaciones().stream().anyMatch(notificacion-> notificacion.getMensaje().equals(textoDeRecomendacion)));
	}
	
	@Test
	//recomienda aquellas peliculas que fueron evaluadas por 2 o más amigos con puntaje 3 o superior, ordenadas por el rating de IMDB. 
	public void test02CuandoSeRecomiendaPorIDBMAUnUsuarioSeObtieneLasPeliculasQueAlMenosDosAmigosCalificaronCon3OMasYSeLasOrdenaPorElRatingDeIMDB() {
		//SetUp: Se guarda las reproducciones individuales que reciben los usuarios cuando le indican a moovies que quieren ver una pelicula
		Reproduccion reproduccionDeDuroDeMatarDeGandalf = moovies.verPelicula(durodematar);
		Reproduccion reproduccionDeDuroDeMatarDeTrump = moovies.verPelicula(durodematar);
		Reproduccion reproduccionDeDuroDeMatarDeMrT = moovies.verPelicula(durodematar);
		Reproduccion reproduccionDeDuroDeMatarDeOdin = moovies.verPelicula(durodematar);
				
		Reproduccion reproduccionDePokemonDeGandalf = moovies.verPelicula(pokemon);
		Reproduccion reproduccionDePokemonDeTrump = moovies.verPelicula(pokemon);
		Reproduccion reproduccionDePokemonDeMrT = moovies.verPelicula(pokemon);
		Reproduccion reproduccionDePokemonDeOdin = moovies.verPelicula(pokemon);
		
		Reproduccion reproduccionDeTerminatorDeGandalf = moovies.verPelicula(terminator);
		Reproduccion reproduccionDeTerminatorDeTrump = moovies.verPelicula(terminator);
		Reproduccion reproduccionDeTerminatorDeMrT = moovies.verPelicula(terminator);
		Reproduccion reproduccionDeTerminatorDeOdin = moovies.verPelicula(terminator);
		

		moovies.darlePlayAReproduccion(reproduccionDeDuroDeMatarDeMrT, mrT);
		moovies.darlePlayAReproduccion(reproduccionDeDuroDeMatarDeTrump, trump);
		moovies.darlePlayAReproduccion(reproduccionDeDuroDeMatarDeGandalf, gandalf);
		moovies.darlePlayAReproduccion(reproduccionDeDuroDeMatarDeOdin, odin);
		
		moovies.darlePlayAReproduccion(reproduccionDePokemonDeMrT, mrT);
		moovies.darlePlayAReproduccion(reproduccionDePokemonDeTrump, trump);
		moovies.darlePlayAReproduccion(reproduccionDePokemonDeGandalf, gandalf);
		moovies.darlePlayAReproduccion(reproduccionDePokemonDeOdin, odin);
		
		moovies.darlePlayAReproduccion(reproduccionDeTerminatorDeMrT, mrT);
		moovies.darlePlayAReproduccion(reproduccionDeTerminatorDeTrump, trump);
		moovies.darlePlayAReproduccion(reproduccionDeTerminatorDeGandalf, gandalf);
		moovies.darlePlayAReproduccion(reproduccionDeTerminatorDeOdin, odin);
		

		moovies.calificarPelicula(mrT, durodematar,1,"Bruce willis es un pescao");
		moovies.calificarPelicula(odin, durodematar,2,"Bruce willis es un pescao");
		moovies.calificarPelicula(trump, durodematar,2,"Bruce willis es un pescao");
		moovies.calificarPelicula(gandalf, durodematar,1,"Bruce willis es un pescao");
		
		moovies.calificarPelicula(mrT, pokemon,3,"Bruce willis es un pescao");
		moovies.calificarPelicula(odin, pokemon,3,"Bruce willis es un pescao");
		moovies.calificarPelicula(trump, pokemon,1,"Bruce willis es un pescao");
		moovies.calificarPelicula(gandalf, pokemon,1,"Bruce willis es un pescao");
		
		moovies.calificarPelicula(mrT, terminator,2,"Bruce willis es un pescao");
		moovies.calificarPelicula(odin, terminator,1,"Bruce willis es un pescao");
		moovies.calificarPelicula(trump, terminator,1,"Bruce willis es un pescao");
		moovies.calificarPelicula(gandalf, terminator,1,"Bruce willis es un pescao");
		
		pepita.setRecomendacion(2);
		
		String textoDeRecomendacion = "Moovies aprecia tu subscripcion y conociendote como nadie mas, esta temporada te recomienda: pokemon, ";
		//assertTrue(moovies.recomendarUsuario(pepita).equals(textoDeRecomendacion));
		moovies.recomendarUsuario(pepita);
		verify(moovies).recomendarUsuario(pepita);
		assertTrue(pepita.getColeccionDeNotificaciones().stream().anyMatch(notificacion-> notificacion.getMensaje().equals(textoDeRecomendacion)));
	}

}
