package unq.moovies.SistemaVerPelicula;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;

import unq.moovies.Genero.Genero;
import unq.moovies.project.Moovies;
import unq.moovies.project.Pelicula;
import unq.moovies.project.ProfileUsuario;
import unq.moovies.project.PublicInfo;
import unq.moovies.project.Reproduccion;
import unq.moovies.project.Usuario;

public class TestDeVerPelicula {
	
	// Instancias que se utilizan en el test.

	private Moovies 		moovies;
	private Usuario 		pepita;
	private Pelicula 		duroDeMatar;
	private Reproduccion 	reproduccionDeDuroDeMatar;
	private ProfileUsuario 	mockPerfil;
	private PublicInfo		mockPublicInfo;
	private Genero			mockGenero;
	//Setup del test, se Inicia el sistema moovies, y una instancia de usuario con su nombre. Despues se inician las instancias de peliculas con sus nombres y se agregan al sistema.
	
	@Before

	public void setup(){
		this.mockPerfil							= mock(ProfileUsuario.class);
		this.mockPerfil							= mock(ProfileUsuario.class);
		this.mockGenero							= mock(Genero.class);
		this.moovies 							= new Moovies();
		this.pepita 							= new Usuario("Pepita","golondrina",this.mockPerfil,"1");
		this.reproduccionDeDuroDeMatar 			= new Reproduccion(120);
		this.duroDeMatar 						= new Pelicula(this.mockPublicInfo,this.mockGenero,this.reproduccionDeDuroDeMatar);
		
		this.moovies.agregarUsuario(pepita);
		this.moovies.agregarPelicula(duroDeMatar);
	}
	
	
	@Test
	public void test00_Una_Reproduccion_Tiene_Una_Duracion_En_Minutos() {
		//setUp
		Integer resultadoObtenido = duroDeMatar.getDuracion();
		Integer resultadoEsperado= 120;
		
		//asercion
		assertEquals(resultadoObtenido, resultadoEsperado);
	}
	
	//Como precondicion el usuario ya tiene que haber buscado y elegido la pelicula, por lo 
	//cual no puede pedirle a moovies una pelicula que no este en la base de datos.
	@Test
	public void test01_Cuando_Un_Usuario_Le_Pide_A_Moovies_Ver_Pelicula_Moovies_Le_Devuelve_Su_Reproduccion() {
		//setUp
		Reproduccion resultadoObtenido =moovies.verPelicula(duroDeMatar);
		Reproduccion resultadoEsperado= duroDeMatar.getReproduccion();
		
		//asercion
		assertEquals(resultadoObtenido, resultadoEsperado);
	}

	@Test
	public void test02_Cuando_Un_Usuario_Le_Da_Play_A_Una_Pelicula_El_Sistema_La_Reproduce_Hasta_Terminarla() {
	
		//setup:  Se guarda la reproduccion individual que recibe pepita cuando le indica a moovies que quieren ver una pelicula.
		Reproduccion reproduccionDeDuroDeMatarDePepita = moovies.verPelicula(duroDeMatar);
		
		//ejecucion:Le da play a la reproduccione obtenida.
		moovies.darlePlayAReproduccion(reproduccionDeDuroDeMatarDePepita, pepita);
		
		//asercion
		assertTrue (reproduccionDeDuroDeMatarDePepita.reproduccionterminada());
	}
	
	
	@Test
	public void test03_Cuando_Un_Usuario_Le_Da_Play_A_Una_Reproduccion_Puede_Elegir_Pausar_la_En_Un_Segundo_De_Su_Duracion() {

		//setup: Se guarda la reproduccion individual que recibe pepita cuando le indica a moovies que quieren ver una pelicula.
		Reproduccion reproduccionDeDuroDeMatarDePepita = moovies.verPelicula(duroDeMatar);
		
		//ejecucion: Le da play a la reproduccione obtenida hasta un minuto en especifico.
		moovies.darlePlayHastaMinuto(reproduccionDeDuroDeMatarDePepita,100);
		
		Integer resultadoObtenido = reproduccionDeDuroDeMatarDePepita.getMinutoActual();
		Integer resultadoEsperado = 100;
	
		//asercion
		assertEquals(resultadoObtenido, resultadoEsperado);
	}
	
	@Test
	public void test04_Cuando_Un_Usuario_Elige_Pausar_Una_Reproduccion_En_Un_Segundo_Que_Esta_Fuera_De_Su_Duracion_El_Sistema_Devuelve_La_Reproduccion_Como_Estaba() {
		//setUp: Se guarda la reproduccion individual que recibe pepita cuando le indica a moovies que quieren ver una pelicula.
		Reproduccion reproduccionDeDuroDeMatarDePepita = moovies.verPelicula(duroDeMatar);
		
		//ejecucion: Le da play a la reproduccione obtenida hasta un minuto fuera del rango de duracion de la pelicula.
		moovies.darlePlayHastaMinuto(reproduccionDeDuroDeMatarDePepita,200);
		
		Integer resultadoObtenido = reproduccionDeDuroDeMatarDePepita.getMinutoActual();
		Integer resultadoEsperado = 0;
	
		//asercion
		assertEquals(resultadoObtenido, resultadoEsperado);
	}
	
	@Test
	public void test05_Cuando_Un_Usuario_Termina_Una_Reproduccion_El_Sistema_Moovies_Registra_Y_Sabe_Que_Vio_La_Pelicula() {
		//setup: Se guarda la reproduccion individual que recibe pepita cuando le indica a moovies que quieren ver una pelicula.
		Reproduccion reproduccionDeDuroDeMatarDePepita = moovies.verPelicula(duroDeMatar);
		
		//ejecucion: Le da play a la reproduccione obtenida hasta un minuto fuera del rango de duracion de la pelicula.
		moovies.darlePlayAReproduccion(reproduccionDeDuroDeMatarDePepita, pepita);
		
		//asercion
		assertTrue(moovies.usuarioVioLaPelicula(pepita,duroDeMatar));
	}
}
