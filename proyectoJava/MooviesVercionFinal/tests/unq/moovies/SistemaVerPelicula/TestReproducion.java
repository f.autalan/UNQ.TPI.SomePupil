package unq.moovies.SistemaVerPelicula;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


import unq.moovies.project.Reproduccion;
import static org.mockito.Mockito.*;

public class TestReproducion {

	//Estructura
	private Reproduccion unaReproduccionTest;

	
	@Before
	public void setUp() throws Exception {
		
		this.unaReproduccionTest=spy(new Reproduccion(120));
	}

	@Test
	public void test0000LaDuracionDeLaReproduccionEs120() {
		assertTrue(this.unaReproduccionTest.getDuracion().equals(120));
		
	}
	@Test
	public void test0001AUnaReproduccionSePuedeDecirReproduciteHastaElFinal() {
		this.unaReproduccionTest.reproducirHastaElFinal();
		verify(this.unaReproduccionTest).reproducirHastaElFinal();
		
		assertTrue(this.unaReproduccionTest.getMinutoActual().equals(120));
	}
	@Test
	public void test0002LaReproduccionSabeSiTerminoLaPelicula() {
		this.unaReproduccionTest.reproducirHastaElFinal();
		assertTrue(this.unaReproduccionTest.reproduccionterminada());
		
	}
	@Test
	public void test0003LaReproduccionSePuedeCorrerHastaElMinuto() {
		
		this.unaReproduccionTest.reproducirHastaMinuto(90);
		verify(this.unaReproduccionTest).reproducirHastaMinuto(90);
		
		assertTrue(this.unaReproduccionTest.getMinutoActual().equals(90));
	}
	
	
	
	
	

}
