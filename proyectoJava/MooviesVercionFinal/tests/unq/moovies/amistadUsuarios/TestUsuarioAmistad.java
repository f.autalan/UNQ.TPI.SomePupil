package unq.moovies.amistadUsuarios;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import unq.moovies.project.ProfileUsuario;
import unq.moovies.project.Usuario;


public class TestUsuarioAmistad {
	private Usuario unUsuarioTest;
	private Usuario unUsuarioMock;
	
	private ProfileUsuario unProfileUsuarioMock;
	
	
	
	@Before
	public void setUp() throws Exception {
		this.unUsuarioMock=mock(Usuario.class);
		this.unProfileUsuarioMock=mock(ProfileUsuario.class);
		
		this.unUsuarioTest= spy(new Usuario("Cara","Perro",this.unProfileUsuarioMock,"1"));
				
	}

	@Test
	public void test0000Geteters() {
		
		assertTrue(this.unUsuarioTest.getNick().equals("Cara"));
		assertTrue(this.unUsuarioTest.getContrasenia().equals("Perro"));
		assertTrue(this.unUsuarioTest.getProfile().equals(this.unProfileUsuarioMock));
	}
	@Test
	public void test0001unUsuarioPuedeEstablecerAmistadConOtroUsuario() {
		this.unUsuarioTest.establecerAmistad(this.unUsuarioMock);
		verify(this.unUsuarioTest).establecerAmistad(this.unUsuarioMock);
		assertEquals(1,this.unUsuarioTest.getColeccionDeAmigos().size());

	}
	@Test
	public void test0002unUsuarioPuedeBorrarAOtroUsarioDeSuColeccionDeAmistad() {
		this.unUsuarioTest.establecerAmistad(this.unUsuarioMock);
		this.unUsuarioTest.borrarAmistad(this.unUsuarioMock);
		verify(this.unUsuarioTest).borrarAmistad(this.unUsuarioMock);
		assertEquals(0,this.unUsuarioTest.getColeccionDeAmigos().size());

	}
	
	
	

}
