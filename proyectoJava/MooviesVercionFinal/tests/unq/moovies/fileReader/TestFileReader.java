package unq.moovies.fileReader;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import unq.moovies.project.Moovies;
import unq.moovies.project.Pelicula;
import unq.moovies.project.Usuario;
import unq.moovies.sistemaDeCalificacion.Calificacion;


public class TestFileReader {

	
	private PeliculaFileReader fileReaderDePelicula;
	private List<Pelicula> peliculasCargadosDelFileReader;
	private Pelicula peli;
	private UsuarioFileReader fileReaderDeUsuario;
	private List<Usuario> usuariosCargadosDelFileReader;
	private Usuario  usuarioTest;
	private RatingFileReader fileReaderDeCalificaciones;
	private List<Calificacion> calificacionesCargadasDelFileReader;
	private Calificacion calificacionTest;
	private Moovies unMoovies;
	
	@Before
	public void setup(){
		this.fileReaderDeUsuario = new UsuarioFileReader ("/home/fernando/data/u.user.csv");
		this.fileReaderDePelicula= new PeliculaFileReader ("/home/fernando/data/u.item.new.csv");
		this.fileReaderDeCalificaciones= new RatingFileReader ("/home/fernando/data/u.data.new.csv");
		this.peliculasCargadosDelFileReader= fileReaderDePelicula.readFile();
		this.usuariosCargadosDelFileReader = fileReaderDeUsuario.readFile();
		this.calificacionesCargadasDelFileReader= fileReaderDeCalificaciones.readFile();
		
		unMoovies = new Moovies();
		
		
	}

	@Test
	public void test0001LaListaTieneAPeliculaDeNombreGoldenEye() {
		
				
		assertTrue(this.peliculasCargadosDelFileReader.stream().anyMatch(pelicula -> pelicula.getNombre().equals(" GoldenEye (1995) ")));
	
	}
	
	@Test
	public void test0002LaPrimerPeliculaQueCargaElfileReaderEsToyStoryTiene3GenersSonAnimationChildrensYComedy() {
		
		peli=peliculasCargadosDelFileReader.get(0);
		assertTrue(peli.getNombre().equals(" Toy Story (1995) "));
		assertFalse(peli.esDelGenero("unknown"));
		assertFalse(peli.esDelGenero("Action"));
		assertFalse(peli.esDelGenero("Adventure"));
		assertTrue(peli.esDelGenero("Animation"));
		assertTrue(peli.esDelGenero("Children's"));
		assertTrue(peli.esDelGenero("Comedy"));
		assertFalse(peli.esDelGenero("Crime"));
		assertFalse(peli.esDelGenero("Documentary"));
		assertFalse(peli.esDelGenero("Drama"));
		assertFalse(peli.esDelGenero("Fantasy"));
		assertFalse(peli.esDelGenero("Horror"));
		assertFalse(peli.esDelGenero("Musical"));
		assertFalse(peli.esDelGenero("Mystery"));
		assertFalse(peli.esDelGenero("Romance"));
		assertFalse(peli.esDelGenero("Sci-F"));
		assertFalse(peli.esDelGenero("Thriller"));
		assertFalse(peli.esDelGenero("War"));
		assertFalse(peli.esDelGenero("Western"));
		Integer sizeGenero= peli.getPublicInfo().getGeneros().size();
		assertTrue(sizeGenero.equals(3));
	}
	@Test
	public void test0003LaPrimerPeliculaSeEstrenoEn01Jan1995YSuIdEstt0114709() {
		
		peli = peliculasCargadosDelFileReader.get(0);		
		assertTrue(peli.getFechaDeEstreno().equals(" 01-Jan-1995 "));
		assertTrue(peli.getId().equals(" tt0114709 "));
	}

	
	@Test
	public void test0004LaListaTieneAUnUsuarioDeNombreAngeloGlover() {
		
		
		assertTrue(this.usuariosCargadosDelFileReader.stream().anyMatch(usuario -> usuario.getNombre().equals("Aaron")));
	
	}
	
	@Test
	public void test0005ElPerfilDelUsuarioEstaCompuestoPorSuNombreAaronAppellidoPooleEdad24OcupaciontechnicianYCodigoPostal85711() {
		
		usuarioTest = usuariosCargadosDelFileReader.get(0);		
		assertTrue(usuarioTest.getProfile().getNombre().equals("Aaron"));
		assertTrue(usuarioTest.getProfile().getApellido().equals("Poole"));
		assertTrue(usuarioTest.getProfile().getEdad().equals("24"));
		assertTrue(usuarioTest.getProfile().getOcupacion().equals("technician"));
		assertTrue(usuarioTest.getProfile().getCodigoPostal().equals("85711"));
		assertTrue(usuarioTest.getId().equals("1"));
	}
	
	@Test
	public void test0006LaListaTieneAUnUserIdDeNumero290() {
		assertTrue(this.calificacionesCargadasDelFileReader.stream().anyMatch(calificacion -> calificacion.getIdUsuario().equals("290")));
	}

	@Test
	public void test0007LaCalificacionDelUsuarioDeId290EstaCompuestaPorSuIdUnTimeStampElIdDeLaPeliQueCalificoySuRating() {
		
		calificacionTest = calificacionesCargadasDelFileReader.get(33);		
		assertTrue(calificacionTest.getIdUsuario().equals("290"));
		assertTrue(calificacionTest.getRating().equals(4));
		assertTrue(calificacionTest.getTimeStamp().equals("880731963"));
		assertTrue(calificacionTest.getIdPelicula().equals("88"));
	}
	
	@Test
	public void test008MooviesImportaCorrectamenteLosArchivosDeLaBaseDeDatos() {
		unMoovies.setRutaPeliculas("/home/fernando/data/u.item.new.csv");
		unMoovies.setRutaUsuarios("/home/fernando/data/u.user.csv");
		unMoovies.setRutaCalificaciones("/home/fernando/data/u.data.new.csv");
		unMoovies.importarPeliculas();
		unMoovies.importarUsuarios();
		unMoovies.importarCalificaciones();
		
		assertFalse (unMoovies.buscarPelicula(" GoldenEye (1995) ").isEmpty());
		assertFalse (unMoovies.buscarUsuario("Aaron").isEmpty());
	}
}
