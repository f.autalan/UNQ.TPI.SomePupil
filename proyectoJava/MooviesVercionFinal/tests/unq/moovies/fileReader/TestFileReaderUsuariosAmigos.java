package unq.moovies.fileReader;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import unq.moovies.project.Amistad;
import unq.moovies.project.Moovies;
import unq.moovies.project.Usuario;

public class TestFileReaderUsuariosAmigos {

	private AmitadFileReader amistadFileRead;
	private List<Amistad> amistadCargadosDelFileReader;
	private Moovies moovis;
	@Before
	public void setup(){
		
		this.amistadFileRead=new AmitadFileReader("/home/fernando/data/u.connection.csv");
		this.amistadCargadosDelFileReader= amistadFileRead.readFile();
		this.moovis=new Moovies();
		this.moovis.setRutaUsuarios("/home/fernando/data/u.user.csv");
		this.moovis.setRutaAmistades("/home/fernando/data/u.connection.csv");
		this.moovis.importarUsuarios();
		this.moovis.importarAmistad();
		
		
		
	}
		
	@Test
	public void test0000EnLaColleccionDeAmistadesExisteUnIdEmesior1YIdRemisor20(){
		assertTrue(this.amistadCargadosDelFileReader.stream().anyMatch(amistad -> amistad.getIdEmisor().equals("1") && amistad.getIdReceptor().equals("20")));

	}
	@Test
	public void test0001EnLaColleccionDeAmistadesNoExisteUnIdEmesior20YIdRemisor1(){
		assertFalse(this.amistadCargadosDelFileReader.stream().anyMatch(amistad -> amistad.getIdEmisor().equals("20") && amistad.getIdReceptor().equals("1")));

	}
	
	
	@Test 
	public void test0002EnLaColleccionDeAmistadesExisteUnaConElIdEmisor939YIdReseptor940(){
		assertTrue(this.amistadCargadosDelFileReader.stream().anyMatch(amistad -> amistad.getIdEmisor().equals("939") && amistad.getIdReceptor().equals("940")));
	}
	
	@Test 
	public void test0003EnLaColleccionDeAmistadesNoExisteUnaConElIdEmisor940YIdReseptor939(){
		assertFalse(this.amistadCargadosDelFileReader.stream().anyMatch(amistad -> amistad.getIdEmisor().equals("940") && amistad.getIdReceptor().equals("939")));
	}
	
	

	@Test 
	public void test0004ElUsuarioConId1EsAmigoDelUsuarioConId20YElUsuario20EsAmigoDelUsuario1(){
		Usuario emisor=this.moovis.buscarUsuarioPorId("1");
		
		Usuario receptor=this.moovis.buscarUsuarioPorId("57");

		assertTrue(emisor.getColeccionDeAmigos().contains(receptor));
		assertTrue(receptor.getColeccionDeAmigos().contains(emisor));

		
	}
	
	
	@Test 
	public void test0005ElUsuarioConId939EsAmigoDelUsuarioConId940YElUsuario940EsAmigoDelUsuario939(){
		Usuario emisor=this.moovis.buscarUsuarioPorId("939");
		Usuario receptor=this.moovis.buscarUsuarioPorId("940");
		assertTrue(emisor.getColeccionDeAmigos().contains(receptor));
		assertTrue(receptor.getColeccionDeAmigos().contains(emisor));
		
	}

	
	

}
