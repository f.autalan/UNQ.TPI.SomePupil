package unq.moovies.project;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import unq.moovies.Genero.Genero;

public class TestPublicInfoDePelicula {

	// Instancias que se utilizan en el test.
	
	private ArrayList<String> 	listaDeGenerosDeRushHour;
	private PublicInfo 			informacionPublicaDeRushHour;
	private Reproduccion 		reproduccionDeRushHour;
	private Pelicula 			rushHour;
	private Genero 	 			unMockGenero;
	
	//Setup del test, se construye la pelicula con una reproduccion de 150 minutos, y una informacion publica.
	
	@Before
	public void setUp(){
	    this.unMockGenero		 = mock(Genero.class);
	    this.listaDeGenerosDeRushHour = new ArrayList<String>();
	    this.listaDeGenerosDeRushHour.add("Comedia");
	    this.listaDeGenerosDeRushHour.add("Accion");
	    this.informacionPublicaDeRushHour = new PublicInfo("Rush Hour", "5/08/2002","2323", this.listaDeGenerosDeRushHour);
	    this.reproduccionDeRushHour = new Reproduccion(150);
	    this.rushHour= new Pelicula(this.informacionPublicaDeRushHour,this.unMockGenero,this.reproduccionDeRushHour);
	}
	
	@Test
	public void test0001_El_Nombre_De_La_Pelicula_Es_RushHour() {
		//set up
		String nombreDeLaPelicula=rushHour.getNombre();
		String nombreEsperado="Rush Hour";
		
		//asercion
		assertEquals(nombreEsperado,nombreDeLaPelicula);
	}
	@Test
	public void test0002_La_Fecha_De_Estreno_De_La_Pelicula_Es_Cinco_Del_Ocho_Del_Dosmildos() {
		//set up
		String fechaDeEstreno=rushHour.getFechaDeEstreno();
		String fechaEsperada="5/08/2002";
		
		//asercion
		assertEquals(fechaEsperada,fechaDeEstreno);
	}
	@Test
	public void test0003_El_ID_De_La_Pelicula_Es_2323(){
		//set up
		String idObtenido= rushHour.getId();
		String idEsperado="2323";
		
		//asercion
		assertEquals(idObtenido , idEsperado);
		
	}
	
	@Test
	public void test0004_La_Pelicula_Es_De_Genero_Comedia_Y_De_Genero_Accion(){
		assertTrue((rushHour.esDelGenero("Comedia") && rushHour.esDelGenero("Accion")));
	}
	
	
}
