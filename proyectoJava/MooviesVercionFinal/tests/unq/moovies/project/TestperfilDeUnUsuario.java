package unq.moovies.project;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import unq.moovies.project.ProfileUsuario;

public class TestperfilDeUnUsuario {
	
	// Instancias que se utilizan en el test.
	
	private ProfileUsuario perfil;
	
	//Setup del test, se construye un perfil de usuario.
	
	@Before
	public void setUp(){
		this.perfil = new ProfileUsuario("Fernando","Autalan","23","1856","Estudiante");
	}
	
	
	
	@Test
	public void test0001_El_Nombre_Del_Duenio_Del_Perfil_Es_Fernando() {
		//set up 
		String nombrePerfil=this.perfil.getNombre();
		String nombreEsperado="Fernando";
		
		//asercion
		assertEquals(nombreEsperado,nombrePerfil);
	}
	@Test
	public void test0002_El_Apellido_Del_Duenio_Del_Perfil_Es_Autalan() {
		//set up 
		String apellidoPerfil=this.perfil.getApellido();
		String apellidoEsperado="Autalan";
        
		//asercion
		assertEquals(apellidoEsperado,apellidoPerfil);
	}
	
	@Test
	public void test0003_La_Edad_Del_Duenio_Del_Perfil_Es_23(){
		//set up
		
		String edadPerfil=this.perfil.getEdad();
		String edadEsperada="23";
		
		//asercion
		assertEquals(edadEsperada , edadPerfil);
		
	}
	@Test
	public void test0004_La_Ocupacion_Del_Duenio_Del_Perfil_Es_Estudiante(){
		//set up
		String ocupacionDelPerfil=this.perfil.getOcupacion();
		String ocupacionEsperada="Estudiante";
		
		//asercion
		assertEquals(ocupacionEsperada,ocupacionDelPerfil);
		
		
	}
	@Test
	public void test0005_El_Codigo_Postal_Del_Duenio_Del_Pefil_Es_1856(){
		//set up
		String codigoPostalDelPefil=this.perfil.getCodigoPostal();
		String codigoPostalEsperado="1856";
		
		//asercion
		assertEquals(codigoPostalEsperado,codigoPostalDelPefil);
			
	}
}
