package unq.moovies.sistemaDeAdministradores;

import static org.junit.Assert.*;

import static org.mockito.Mockito.*;
import org.junit.Before;
import org.junit.Test;

import unq.moovies.Genero.Genero;
import unq.moovies.project.Moovies;
import unq.moovies.project.Pelicula;
import unq.moovies.project.ProfileUsuario;
import unq.moovies.project.PublicInfo;
import unq.moovies.project.Reproduccion;
import unq.moovies.project.Usuario;

public class TestAdministradores {

	// Instancias que se utilizan en el test.
	
	private Moovies moovies;
	private Usuario 			pepita;
	private Pelicula 			duroDeMatar;
	private ProfileUsuario		mockPerfil;
	private PublicInfo			mockPublicInfo;
	private Genero				mockGenero;
	private Reproduccion		mockReproduccion;
	
	//Setup del test, se Inicia el sistema moovies, y una instancias de usuario con su nombre. Despues se inician las instancias de peliculas con sus nombres y se agregan al sistema.
	@Before
	
	public void setup(){
		this.mockPerfil							= mock(ProfileUsuario.class);
		this.mockGenero							= mock(Genero.class);
		this.mockPublicInfo						= mock(PublicInfo.class);
		this.mockReproduccion					= mock(Reproduccion.class);
		
		this.moovies 							= new Moovies();
		this.pepita 							= new Usuario("Pepita","golondrina",this.mockPerfil,"1");
		this.duroDeMatar 						= new Pelicula(this.mockPublicInfo,this.mockGenero,this.mockReproduccion);
		
		this.moovies.agregarUsuario(pepita);
		this.moovies.agregarPelicula(duroDeMatar);
	
	}
	

	
	// En el setup agregamos a pepita a moovies. Nos fijamos si Moovies sabe que tiene a un usuario registrado.
	
	@Test
	public void test00_Moovies_Puede_Agregar_Usuarios_Y_Saber_Si_Tiene_Registrado_A_Un_Usuario() {
		assertTrue( moovies.tieneRegistradoAUsuario(pepita) );
	}



	// En el setup agregamos a Duro de matar a moovies. Nos fijamos si Moovies sabe que tiene la pelicula registrada.
	@Test
	public void test01_Moovies_Puede_Agregar_Peliculas_Y_Saber_Si_Tiene_Registrado_A_Una_Pelicula() {
		assertTrue( moovies.tieneRegistradoAPelicula(duroDeMatar) );
	}
}
