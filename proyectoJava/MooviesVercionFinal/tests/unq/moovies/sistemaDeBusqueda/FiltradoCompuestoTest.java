package unq.moovies.sistemaDeBusqueda;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import unq.moovies.project.Pelicula;
import unq.moovies.project.ProfileUsuario;
import unq.moovies.project.PublicInfo;
import unq.moovies.project.Usuario;

public class FiltradoCompuestoTest {
	private Usuario usuario;
	private Filtrable filtro1;
	private Filtrable filtro2;
	private Filtrable filtro3;
	private Filtrable filtro4;
	private Filtrable filtro5;
	private Pelicula peli1;
	private Pelicula peli2;
	private Pelicula peli3;
	private List <Pelicula> pelis;
	private ArrayList<String> generos;
	private ArrayList<String> generos2;
	private PublicInfo info;
	private PublicInfo info2;
	private PublicInfo info3;
	private ProfileUsuario mockPerfil;
	
	private Campo campoDirector;
	private Campo campoNombre;
	private Condicion esIgual;
	
	
	@Before
	public void setUp(){
			this.mockPerfil=mock(ProfileUsuario.class);
			this.usuario = new Usuario ("pepe", "joe",this.mockPerfil,"1");
			this.pelis = new ArrayList <Pelicula>();
			this.generos2= new ArrayList <String>();
			this.generos = new ArrayList <String>();
			this.generos.add("terror");
			this.generos2.add("aventura");
			this.generos.add("drama");
			this.info = new PublicInfo("rambo","111","1980",generos, "campanella");
			this.info2 = new PublicInfo("banieros2","22","1981", generos2,"darin" );
			this.info3 = new PublicInfo("et","22","1981", generos2,"darin" );
			this.peli1 = new Pelicula(info);
			this.peli2 = new Pelicula(info2);
			this.peli3 = new Pelicula(info3);
			this.pelis.add(peli1);
			this.pelis.add(peli2);
			this.pelis.add(peli3);
			

			this.campoDirector= new Director();
			this.campoNombre= new Nombre();
			
			this.esIgual= new EsIgual();
			
			
			this.filtro1 = new FiltradoSimple(this.campoDirector,"darin",this.esIgual);
			this.filtro2 = new FiltradoSimple(this.campoNombre, "banieros2", this.esIgual);
			this.filtro3 = new FiltradoAND(this.filtro1,this.filtro2);
			this.filtro4 = new FiltradoSimple(this.campoNombre,"rambo",this.esIgual);
			this.filtro5 = new FiltradoOR(this.filtro1,this.filtro4);
		
			
	}
		
		@Test
		public void test001ALTestearSiHayUnaPeliculaQueSeLLameBanieros2() {
			List <Pelicula> pelis1 = new ArrayList <Pelicula>();
			pelis1 = usuario.filtrar(filtro2, pelis);
			assertEquals(pelis1.size(),1);
		}
	
		@Test
		public void test002TesteoQueSeCumplaQueElDirectorEsDarinySeLLamaBanieros2YDevuelva1ListaCOn1Pelicula() {
			List <Pelicula> pelis1 = new ArrayList <Pelicula>();
			pelis1 = usuario.filtrar(filtro3, pelis);
			assertEquals(pelis1.size(),1);
		}
		
		@Test
		public void test003TesteoQueSeCumplaQueElDirectorEsDarinOSeLLamaRamboYDevuelvaListaCon2Peliculas() {
			List <Pelicula> pelis1 = new ArrayList <Pelicula>();
			pelis1 = usuario.filtrar(filtro5, pelis);
			//List <Pelicula> pelis1 = filtro2.ejecutarFiltrado(pelis);
			assertEquals(pelis1.size(),2);
		}
		

		
}
