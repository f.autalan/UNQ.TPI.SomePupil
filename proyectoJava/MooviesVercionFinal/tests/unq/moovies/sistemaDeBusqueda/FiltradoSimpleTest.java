package unq.moovies.sistemaDeBusqueda;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import unq.moovies.project.Pelicula;
import unq.moovies.project.ProfileUsuario;
import unq.moovies.project.PublicInfo;
import unq.moovies.project.Usuario;

public class FiltradoSimpleTest {
	private Usuario usuario;
	private Usuario usuario2;
	private FiltradoSimple filtro1;
	private FiltradoSimple filtro2;
	private Pelicula peli1;
	private Pelicula peli2;
	private Pelicula peli3;
	private List <Pelicula> pelis;
	private ArrayList<String> generos;
	private ArrayList<String> generos2;
	private PublicInfo info;
	private PublicInfo info2;
	private PublicInfo info3;
	private ProfileUsuario mockPerfil1;
	private ProfileUsuario mockPerfil2;
	private Campo campoDirector;
	private Campo campoNombre;
	private Condicion esIgual;
	private Condicion contiene;
	
	@Before
	public void setUp(){
		this.mockPerfil1=mock(ProfileUsuario.class);
		this.usuario = new Usuario ("pepe", "joe",this.mockPerfil1,"1");
		this.mockPerfil2=mock(ProfileUsuario.class);
		this.usuario2 = new Usuario ("lala", "pass",this.mockPerfil2,"2");
		this.pelis = new ArrayList <Pelicula>();
		this.generos2= new ArrayList <String>();
		this.generos = new ArrayList <String>();
		this.generos.add("terror");
		this.generos.add("suspenso");
		this.generos2.add("aventura");
		this.info = new PublicInfo("rambo","111","1980",generos, "campanella");
		this.info2 = new PublicInfo("et","22","1981", generos2,"darin" );
		
		this.peli1 = new Pelicula(info);
		this.peli2 = new Pelicula(info2);
		this.peli3 = new Pelicula(info2);
		this.pelis.add(peli1);
		this.pelis.add(peli2);
		
		this.campoDirector= new Director();
		this.campoNombre= new Nombre();
		
		this.contiene= new Contiene();
		this.esIgual= new EsIgual();
		
		this.filtro1 = new FiltradoSimple(this.campoDirector,"darin",this.esIgual);
		this.filtro2 = new FiltradoSimple(this.campoNombre,"et",this.contiene);
		//usuario.filtrar(filtro1, pelis);
		 
	}
	
	@Test
	public void test001PreguntoCuantasPelisTienenDirectorIgualDarin() {
		List <Pelicula> pelis1 = usuario.filtrar(filtro1, pelis);
		assertEquals(pelis1.size(),1);
	}
	
	@Test
	public void test002PreguntoCuantasPeliscontieneenSuNombrAET() {
		List <Pelicula> pelis2 = usuario2.filtrar(filtro2, pelis);
		assertEquals(pelis2.size(),1);
	}
	

}
