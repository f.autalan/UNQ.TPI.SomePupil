package unq.moovies.sistemaDeBusqueda;

import static org.junit.Assert.*;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import unq.moovies.project.Moovies;
import unq.moovies.project.ProfileUsuario;
import unq.moovies.project.Usuario;

public class SeleccionarUsuarioTest {
	
	// Instancias que se utilizan en el test.
	
	private Moovies moovies;
	private Usuario pepita;
	private Usuario dragon;
	private Usuario dragon007;
	private Usuario dragoncito;
	private Usuario xx_xxDragonxx_xx;
	private Usuario dumbledore;
	private Usuario dionisia;
	
	//Setup del test, se Inicia el sistema moovies, y las instancias de usuario, cada una con su nombre. Despues se agregan los usuarios al sistema.
	
	@Before
	public void setup(){
		moovies = new Moovies();
		
		ProfileUsuario pepitaProfile = new ProfileUsuario("Pepita"," "," "," "," ");
		ProfileUsuario dragonProfile = new ProfileUsuario("Dragon"," "," "," "," ");
		ProfileUsuario dragon007Profile = new ProfileUsuario("Dragon007"," "," "," "," ");
		ProfileUsuario dragoncitoProfile = new ProfileUsuario("Dragoncito"," "," "," "," ");
		ProfileUsuario dragonxxProfile = new ProfileUsuario("xx_xxDragonxx_xx"," "," "," "," ");
		ProfileUsuario dumbledoreProfile = new ProfileUsuario("Dumbledore"," "," "," "," ");
		ProfileUsuario dionisiaProfile = new ProfileUsuario("Dionisia"," "," "," "," ");
		
		pepita = new Usuario("Pepita","12",pepitaProfile,"1");
		dragon = new Usuario("Dragon","31",dragonProfile,"2");
		dragon007 = new Usuario("Dragon007","12",dragon007Profile,"3");
		dragoncito = new Usuario("Dragoncito","31",dragoncitoProfile,"4");
		xx_xxDragonxx_xx = new Usuario("xx_xxDragonxx_xx","45",dragonxxProfile,"5" );
		dumbledore = new Usuario("Dumbledore","12",dumbledoreProfile,"6");
		dionisia = new Usuario("Dionisia","12",dionisiaProfile,"7" );
	
		moovies.agregarUsuario(pepita);
		moovies.agregarUsuario(dragon);
		moovies.agregarUsuario(dragon007);
		moovies.agregarUsuario(dragoncito);
		moovies.agregarUsuario(xx_xxDragonxx_xx);
		moovies.agregarUsuario(dumbledore);
		moovies.agregarUsuario(dionisia);
		
	}
	
	// La coleccion de posibles resultados es una coleccion de strings con los nombres que podrian llegar a ser el resultado buscado. El sistema diferencia un resultado posible, fijandose si en el nombre del usuario aparece en alguna parte el nombre buscado.
	// Se espera que cuando pepita busca "Dragon" se le aparezca un ResultadoDeBusqueda en el cual aparecen los usuarios que en su nombre contienen "Dragon" en alguna parte.
	
	@Test
	public void test001_Cuando_Un_Usuario_Busca_Otro_Usuario_En_El_Sistema_Moovies_Recibe_Una_Coleccion_De_Posibles_Resultados() {

		//set up del test 
		
		List<Usuario> resultadoObtenido = moovies.buscarUsuario("Dragon");
				
		//asercion
		assertTrue( resultadoObtenido.contains(dragon) &&
					resultadoObtenido.contains(dragon007)  &&
					resultadoObtenido.contains(dragoncito)  &&
					resultadoObtenido.contains(xx_xxDragonxx_xx));
	}
	
	
	//Ya esta asumido que este mensaje solo puede ser enviado una vez se le devolvio al usuario La lista de Resultados.
	//Asumo que la interfaz de usuario se encargara de que al elegir uno de los resultados se envie el mensaje "elegirusuario" 
	@Test
	public void test003_Cuando_un_usuario_elige_un_Nombre_de_un_resultado_moovies_le_devuelve_el_profile_del_usuario(){
		
		//set up 
		
		ProfileUsuario resultadoObtenido = moovies.elegirUsuario("Dragon007");
		ProfileUsuario resultadoEsperado = dragon007.getProfile();
		
		//asercion
		
		assertEquals(resultadoObtenido,resultadoEsperado);
	}
	
}
