package unq.moovies.sistemaDeBusqueda;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


import org.junit.Before;
import org.junit.Test;

import unq.moovies.project.Pelicula;

public class TestCampoDirector {
	
	private Pelicula 		unMockPelicula;
	private Pelicula 		otroMockPelicula;

	private Director	  	unCampoDirectorTest;
	
	
	@Before
	public void setUp() throws Exception {
		this.unMockPelicula				= mock(Pelicula.class);
		when(this.unMockPelicula.getDirector()).thenReturn("GardelConPeluca");
		
		this.otroMockPelicula				= mock(Pelicula.class);
		when(this.otroMockPelicula.getDirector()).thenReturn("PepitaLaLoca");
		
		this.unCampoDirectorTest		= new Director();
		
	}

	@Test
	public void test() {
		
		String respuesta1= this.unCampoDirectorTest.getCampo(this.unMockPelicula);
		String respuesta2= this.unCampoDirectorTest.getCampo(this.otroMockPelicula);
		
		assertEquals(respuesta1, "GardelConPeluca");
		assertEquals(respuesta2, "PepitaLaLoca");
	}

}
