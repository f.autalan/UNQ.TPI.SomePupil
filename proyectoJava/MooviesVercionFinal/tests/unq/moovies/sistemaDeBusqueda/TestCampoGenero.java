package unq.moovies.sistemaDeBusqueda;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;


import org.junit.Before;
import org.junit.Test;

import unq.moovies.project.Pelicula;

public class TestCampoGenero {
	private Pelicula 			unMockPelicula;
	
	private ArrayList<String>	unaColeccionDeStrings;
	
	private CampoGenero 		unCampoGeneroTest;
	
	
	@Before
	public void setUp() throws Exception {
		this.unaColeccionDeStrings		= new ArrayList<String>();
		this.unaColeccionDeStrings.add("Terror");
		this.unaColeccionDeStrings.add("VampiroChupaSangre");
		
		this.unMockPelicula				= mock(Pelicula.class);
		when(this.unMockPelicula.getGeneros()).thenReturn(this.unaColeccionDeStrings);
		
		this.unCampoGeneroTest			= new CampoGenero();
	}

	@Test
	public void test() {
		
		
		String resultado= this.unCampoGeneroTest.getCampo(this.unMockPelicula);

		assertTrue(resultado.contains("Terror"));
		assertTrue(resultado.contains("VampiroChupaSangre"));
		
	}

}
