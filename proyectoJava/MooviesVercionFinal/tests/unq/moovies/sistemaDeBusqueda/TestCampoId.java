package unq.moovies.sistemaDeBusqueda;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


import org.junit.Before;
import org.junit.Test;

import unq.moovies.project.Identificable;
import unq.moovies.project.Pelicula;
import unq.moovies.project.Usuario;

public class TestCampoId {
	private Identificable 		unMockUsuarioConID1;
	private Identificable 		unMockUsuarioConID2;
	
	private Identificable 		unMockPeliculaConID1;
	private Identificable 		unMockPeliculaConID2;
	
	
	private Id		  			unCampoIDTest;
	
	
	
	@Before
	public void setUp() throws Exception {
		
		this.unMockUsuarioConID1			= mock(Usuario.class);
		when(this.unMockUsuarioConID1.getId()).thenReturn("1");
		
		this.unMockUsuarioConID2			= mock(Usuario.class);
		when(this.unMockUsuarioConID2.getId()).thenReturn("2");
		
		this.unMockPeliculaConID1			= mock(Pelicula.class);
		when(this.unMockPeliculaConID1.getId()).thenReturn("1");
		
		this.unMockPeliculaConID2			= mock(Pelicula.class);
		when(this.unMockPeliculaConID2.getId()).thenReturn("2");
		
	
		this.unCampoIDTest					= new Id();
		
	}

	@Test
	public void test0000ElCampoIdeFiltraUnaListaDePeliculaConCondicionEsIgualA1() {
		
		String respuesta1= this.unCampoIDTest.getCampo(unMockUsuarioConID1);
		String respuesta2=  this.unCampoIDTest.getCampo(unMockUsuarioConID2);
				
		assertEquals(respuesta1, "1");
		assertEquals(respuesta2, "2");
	}
	
	@Test
	public void test0001ElCampoIdeFiltraUnaListaDePeliculaConCondicionEsIgualA1() {
		String respuesta1= this.unCampoIDTest.getCampo(unMockPeliculaConID1);
		String respuesta2= this.unCampoIDTest.getCampo(unMockPeliculaConID2);
		
		assertEquals(respuesta1, "1");
		assertEquals(respuesta2, "2");
	}

}
