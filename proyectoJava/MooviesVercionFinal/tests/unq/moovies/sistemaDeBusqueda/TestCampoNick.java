package unq.moovies.sistemaDeBusqueda;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import unq.moovies.project.Usuario;

public class TestCampoNick {
	
	private Usuario unMockUsuarioNickFernando;
	private Usuario unMOckUsuarioNickIvan;
	
	private Nick unCampoNickTest;
	
	@Before
	public void setUp() throws Exception {
		this.unMockUsuarioNickFernando			= mock(Usuario.class);
		when(this.unMockUsuarioNickFernando.getNick()).thenReturn("Fernando");
		
		this.unMOckUsuarioNickIvan				= mock(Usuario.class);
		when(this.unMOckUsuarioNickIvan.getNick()).thenReturn("Ivan");
		
		
		this.unCampoNickTest					= new Nick();
		
		
	
	}

	@Test
	public void test() {
		
		String resultado1 = this.unCampoNickTest.getCampo(this.unMOckUsuarioNickIvan);
		String resultado2 = this.unCampoNickTest.getCampo(this.unMockUsuarioNickFernando);
		
		
		assertEquals(resultado1, "Ivan");
		assertEquals(resultado2, "Fernando");
		
	}

}
