package unq.moovies.sistemaDeBusqueda;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


import org.junit.Before;
import org.junit.Test;

import unq.moovies.project.Pelicula;
import unq.moovies.project.Usuario;

public class TestCampoNombre {

	private Nombre 			unNombreTest;
	private Usuario 		unMockUsuarioConNombrePepita;
	private Usuario			unMockUsuarioconNombreIonira;

	private Pelicula 		unMockPeliculaConNombreDuroDeMatar;
	private Pelicula 		unMockPeliculaConNombreElSeniorDeLosAnillos;
	
	
	
	@Before
	public void setUp() throws Exception {
		
		this.unMockUsuarioConNombrePepita					= mock(Usuario.class);
		when(this.unMockUsuarioConNombrePepita.getNombre()).thenReturn("Pepita");
		
		this.unMockUsuarioconNombreIonira					= mock(Usuario.class);
		when(this.unMockUsuarioconNombreIonira.getNombre()).thenReturn("Ionira");
		
		
		this.unMockPeliculaConNombreDuroDeMatar					= mock(Pelicula.class);
		when(this.unMockPeliculaConNombreDuroDeMatar.getNombre()).thenReturn("DuroDeMatar");
		
		this.unMockPeliculaConNombreElSeniorDeLosAnillos				= mock(Pelicula.class);
		when(this.unMockPeliculaConNombreElSeniorDeLosAnillos.getNombre()).thenReturn("ElSeniorDeLosAnillos");
		
		

		this.unNombreTest									= new Nombre(); 
								
	}

	@Test
	public void test0000ElCampoNombreDevuelveElNombreDelNombrable(){

		
		String resultadoUsuario1= this.unNombreTest.getCampo(this.unMockUsuarioConNombrePepita);
		String resultadoUsuario2= this.unNombreTest.getCampo(this.unMockUsuarioconNombreIonira);
		
		String resultadoPelicula1= this.unNombreTest.getCampo(this.unMockPeliculaConNombreDuroDeMatar);
		String resultadoPelicula2= this.unNombreTest.getCampo(this.unMockPeliculaConNombreElSeniorDeLosAnillos);
				
	
		
		assertEquals(resultadoUsuario1, "Pepita");
		assertEquals(resultadoUsuario2, "Ionira");
		
		assertEquals(resultadoPelicula1, "DuroDeMatar");
		assertEquals(resultadoPelicula2, "ElSeniorDeLosAnillos");
		
	}
	
	
}
