package unq.moovies.sistemaDeBusqueda;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;


import unq.moovies.sistemaDeCalificacion.Calificacion;

public class TestCampoRainting {

	private Rating 			unRatingTest;
	private Calificacion 	unMockCalificacion;
	@Before
	public void setUp() throws Exception {
		this.unMockCalificacion			= mock(Calificacion.class);
		when(this.unMockCalificacion.getRating()).thenReturn(3);
		this.unRatingTest				= new Rating();
	}

	@Test
	public void test() {
		assertTrue(this.unRatingTest.getCampo(this.unMockCalificacion).equals("3"));
	}

}
