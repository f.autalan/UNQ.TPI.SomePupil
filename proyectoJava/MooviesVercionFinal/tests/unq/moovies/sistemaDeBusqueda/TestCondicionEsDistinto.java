package unq.moovies.sistemaDeBusqueda;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestCondicionEsDistinto {

	private EsDistinto unEsDIstintoTest;
	@Before
	public void setUp() throws Exception {
		this.unEsDIstintoTest= new EsDistinto();
	
	}

	@Test
	public void test0000CuandoAEsDistintoResiveComoValoresPepitaYDionisaYSonDistintos() {
		assertTrue(this.unEsDIstintoTest.cumpleConCondicion("Pepita","Dionisa"));
		
	}

}
