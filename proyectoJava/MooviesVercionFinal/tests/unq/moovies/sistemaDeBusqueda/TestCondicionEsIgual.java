package unq.moovies.sistemaDeBusqueda;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestCondicionEsIgual {
	
	private EsIgual unEsIgualTest;
	@Before
	public void setUp() throws Exception {
	
		this.unEsIgualTest	= new EsIgual();
	}

	@Test
	public void test0000CuandoLaCondicionEsIgualoResiveComoValoresPepitaYPepitaYSonIgual() {
		assertTrue(this.unEsIgualTest.cumpleConCondicion("Pepita","Pepita"));
		
	}

}
