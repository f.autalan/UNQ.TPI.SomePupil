package unq.moovies.sistemaDeBusqueda;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestCondicionEsMayorIgual {
	
	private EsMayorIgual unCondcionEsMayorIgualTest;
	
	@Before
	public void setUp() throws Exception {
		this.unCondcionEsMayorIgualTest		= new EsMayorIgual();
	}

	@Test
	public void test() {
		assertTrue(this.unCondcionEsMayorIgualTest.cumpleConCondicion("1", "1"));
	}

}
