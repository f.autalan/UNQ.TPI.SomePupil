package unq.moovies.sistemaDeCalificacion;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import unq.moovies.Genero.Genero;
import unq.moovies.project.Moovies;
import unq.moovies.project.Pelicula;
import unq.moovies.project.ProfileUsuario;
import unq.moovies.project.PublicInfo;
import unq.moovies.project.Reproduccion;
import unq.moovies.project.Usuario;

public class TestMayoresUsuariosYPeliculas {

	// Instancias que se utilizan en el test.
	
	private Moovies 			moovies;
	private Usuario				pepita;
	private Usuario 			bahamut;
	private Usuario 			tiamat;
	private Usuario 			pelor;
	
	private Pelicula 	 		duroDeMatar;
	private Reproduccion 		reproduccionDeDuroDeMatar;
	
	private Pelicula	 		elSeniorDeLosAnillos;
	private Reproduccion 		reproduccionDelSeniorDeLosAnillos;
	
	private Pelicula 	 		harryPotter;
	private Reproduccion 		reproduccionDeHarryPotter;
	
	private ProfileUsuario 		mockPerfil1;
	private PublicInfo			mockPublicInfo;
	private Genero				mockGenero;

	
	// Set up
	
	@Before
	public void setUp() throws Exception {
		
		//Se instancia moovies, los usuarios y las peliculas con sus respectivas reproducciones.
		this.mockPerfil1						= mock(ProfileUsuario.class);
		this.mockGenero							= mock(Genero.class);
		this.mockPublicInfo						= mock(PublicInfo.class);
		
		this.moovies 							= new Moovies();
		this.pepita 							= new Usuario("Pepita","golondrina",mockPerfil1,"333");
		this.bahamut 							= new Usuario("Bahamut","rhapsodyoffire",mockPerfil1,"234");
		this.tiamat 							= new Usuario("Tiamat","blindguardian123",mockPerfil1,"664");
		this.pelor 								= new Usuario("Pelor","burzum",mockPerfil1,"996");
		
		this.reproduccionDeDuroDeMatar 			= new Reproduccion(120);
		this.duroDeMatar 						= new Pelicula(this.mockPublicInfo,this.mockGenero,reproduccionDeDuroDeMatar);
		
		this.reproduccionDelSeniorDeLosAnillos  = new Reproduccion(240);
		this.elSeniorDeLosAnillos				= new Pelicula(this.mockPublicInfo,this.mockGenero,reproduccionDelSeniorDeLosAnillos);
		
		this.reproduccionDeHarryPotter			= new Reproduccion(100);
		this.harryPotter 						= new Pelicula(this.mockPublicInfo,this.mockGenero,reproduccionDeHarryPotter);
		
		//Se agregan los usuarios y las peliculas al sistema
		
		this.moovies.agregarUsuario(pepita);
		this.moovies.agregarUsuario(tiamat);
		this.moovies.agregarUsuario(pelor);
		this.moovies.agregarUsuario(bahamut);
		this.moovies.agregarPelicula(duroDeMatar);
		this.moovies.agregarPelicula(elSeniorDeLosAnillos);
		this.moovies.agregarPelicula(harryPotter);
		
		//Los usuarios le piden a moovies ver una pelicula y le dan play a la reproduccion que moovies les da. Esto se hace por que sino no pueden calificar las peliculas.
		this.moovies.darlePlayAReproduccion((moovies.verPelicula(duroDeMatar)), pepita);
		this.moovies.darlePlayAReproduccion((moovies.verPelicula(elSeniorDeLosAnillos)), pepita);
		this.moovies.darlePlayAReproduccion((moovies.verPelicula(harryPotter)), pepita);
		
		this.moovies.darlePlayAReproduccion((moovies.verPelicula(duroDeMatar)), bahamut);
		this.moovies.darlePlayAReproduccion((moovies.verPelicula(elSeniorDeLosAnillos)), bahamut);
		this.moovies.darlePlayAReproduccion((moovies.verPelicula(harryPotter)), bahamut);
		
		this.moovies.darlePlayAReproduccion((moovies.verPelicula(duroDeMatar)), pelor);
		this.moovies.darlePlayAReproduccion((moovies.verPelicula(elSeniorDeLosAnillos)), pelor);
		this.moovies.darlePlayAReproduccion((moovies.verPelicula(harryPotter)), pelor);
		
		this.moovies.darlePlayAReproduccion((moovies.verPelicula(duroDeMatar)), tiamat);
		this.moovies.darlePlayAReproduccion((moovies.verPelicula(elSeniorDeLosAnillos)), tiamat);
		this.moovies.darlePlayAReproduccion((moovies.verPelicula(harryPotter)), tiamat);
		
		
		//Los usuarios califican las peliculas con un puntaje y un review.

		this.moovies.calificarPelicula(pepita, duroDeMatar,2,"Bruce willis es un pescao");
		this.moovies.calificarPelicula(pepita, harryPotter,4,"Bruce willis es un pescao");
		
		this.moovies.calificarPelicula(bahamut, elSeniorDeLosAnillos,5,"Bruce willis es un pescao");
		

		this.moovies.calificarPelicula(pelor, duroDeMatar,4,"Bruce willis es un pescao");
		this.moovies.calificarPelicula(pelor, duroDeMatar,1,"Bruce willis es un pescao");
		this.moovies.calificarPelicula(pelor, duroDeMatar,3,"Bruce willis es un pescao");
		this.moovies.calificarPelicula(pelor, harryPotter,4,"Bruce willis es un pescao");
		
		this.moovies.calificarPelicula(tiamat, harryPotter,3,"Bruce willis es un pescao");
	}

	@Test
	public void test00_Moovies_Conoce_Si_Un_Usuario_Esta_En_Los_10_Usuarios_Que_Realizaron_El_Mayor_Numero_De_Calificaciones() {
		assertTrue(moovies.tieneEnLosUsuariosQueRealizaronElMayorNumeroDeCalificacionesA(tiamat));
	}
	
	@Test
	public void test01_Moovies_Conoce_Las_Diez_Peliculas_Que_Tienen_EL_Mayor_Promedio_De_Rating_Ordenado_De_Forma_Descendente() {
	
		//setUp
		ArrayList<Pelicula> peliculasConMayorPromedio = moovies.get10PelisConMayorPromedioDescendente();
		
		//Asercion: Si el orden es el esperado, la pelicula con mayor rating promedio, en este caso, el senior de los anillos, deberia estar primera en la lista.
		//Harry potter segundo, y duro de matar tercero.
		assertEquals(peliculasConMayorPromedio.get(0), elSeniorDeLosAnillos);
		assertEquals(peliculasConMayorPromedio.get(1), harryPotter);
		assertEquals(peliculasConMayorPromedio.get(2), duroDeMatar);
		
	} 
	
	@Test
	public void test02_Moovies_Conoce_Los_Diez_Usuarios_Que_Realizaron_El_Mayor_Numero_De_Calificaciones_De_Forma_Descendente() {

		//setUp
		ArrayList<Usuario> UsuariosConMayorCalificaciones = moovies.get10UsuariosConMayorCalificacionesDescendente();
	
		//Asercion: Si el orden es el esperado, el usuario con mayor cantidad de calificaciones, en este caso, pelor, deberia estar primero en la lista.
		//pepita segunda, y tiamat o bahamut siguiendoles.		
		assertEquals(UsuariosConMayorCalificaciones.get(0), pelor);
		assertEquals(UsuariosConMayorCalificaciones.get(1), pepita);
		assertEquals(UsuariosConMayorCalificaciones.get(2), tiamat);
		assertEquals(UsuariosConMayorCalificaciones.get(3), bahamut);
	}

}
