package unq.moovies.sistemaDeCalificacion;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import unq.moovies.Genero.Genero;
import unq.moovies.project.Moovies;
import unq.moovies.project.Pelicula;
import unq.moovies.project.ProfileUsuario;
import unq.moovies.project.PublicInfo;
import unq.moovies.project.Reproduccion;
import unq.moovies.project.Usuario;

public class TestsDeCalificacionDePelicula {

	// Instancias que se utilizan en el test.

	private Moovies 			moovies;
	private Usuario 			pepita;
	private Usuario 			dionisia;
	private Pelicula 			duroDeMatar;
	private Reproduccion 		reproduccionDeDuroDeMatar;
	private ProfileUsuario 		unMockPerfil;
	private PublicInfo			mockPublicInfo;
	private Genero				mockGenero;
		
	//Setup del test, se Inicia el sistema moovies, y una instancia de usuario con su nombre. Despues se inician las instancias de peliculas con sus nombres y se agregan al sistema.
		
	@Before

	public void setup(){
		this.mockGenero							= mock(Genero.class);
		this.mockPublicInfo						= mock(PublicInfo.class);
		this.unMockPerfil						= mock(ProfileUsuario.class);
		
		this.moovies 							= new Moovies();
		this.pepita 							= new Usuario("Pepita","golondrina",this.unMockPerfil,"23");
		this.dionisia 							= new Usuario("Dionisia","rhapsodyoffire",this.unMockPerfil,"266");
		this.reproduccionDeDuroDeMatar 			= new Reproduccion(120);
		this.duroDeMatar 						= new Pelicula(this.mockPublicInfo,this.mockGenero,reproduccionDeDuroDeMatar);
		
		this.moovies.agregarUsuario(pepita);
		this.moovies.agregarUsuario(dionisia);
		this.moovies.agregarPelicula(duroDeMatar);
	}


	
	@Test
	public void test00_Moovies_Conoce_Las_Calificacion_De_Una_Pelicula() {
		//SetUp: Se guarda las reproducciones individuales que reciben los usuarios cuando le indican a moovies que quieren ver una pelicula
		Reproduccion reproduccionDeDuroDeMatarDePepita = moovies.verPelicula(duroDeMatar);
		Reproduccion reproduccionDeDuroDeMatarDeDionisia = moovies.verPelicula(duroDeMatar);
		
		//Ejecucion:Se les da play a las reproducciones obtenidas y luego se las califica
		

		moovies.darlePlayAReproduccion(reproduccionDeDuroDeMatarDePepita, pepita);
		moovies.darlePlayAReproduccion(reproduccionDeDuroDeMatarDeDionisia, dionisia);
		

		moovies.calificarPelicula(pepita, duroDeMatar,4,"Bruce willis es un pescao");
		moovies.calificarPelicula(dionisia, duroDeMatar,2,"Bruce willis es un pescao");
	
		//Asercion
		assertTrue(moovies.getCalificacionPromedioDePelicula(duroDeMatar)== 3);
	}
	
	@Test
	public void test01_Moovies_Conoce_La_Calificacion_Promedio_Que_Un_Usuario_Le_Dio_A_LasPeliculas() {
		//Setup: Se guarda la reproduccion individual que recibe pepita cuando le indica a moovies que quieren ver una pelicula.
		Reproduccion reproduccionDeDuroDeMatarDePepita = moovies.verPelicula(duroDeMatar);
		
		//Ejecucion: Le da play a la reproduccione obtenida, luego la califica y guardamos las calificaciones promedio de pepita.
		moovies.darlePlayAReproduccion(reproduccionDeDuroDeMatarDePepita, pepita);
		moovies.calificarPelicula(pepita, duroDeMatar,4,"Bruce willis es un pescao");
		
		Integer resultadoObtenido = moovies.getCalificacionesPromedioDeUsuario(pepita);
		Integer resultadoEsperado= 4;
		
		//Asercion.
		assertEquals(resultadoObtenido, resultadoEsperado);
	}
	
	@Test
	public void test02Cuando_Un_Usuario_Le_Envia_A_Moovies_Calificar_Pelicula_Si_Ese_Usuario_No_Vio_La_Pelicula_No_Se_Registra_La_Calificacion() {
		
		//Ejecucion
		moovies.calificarPelicula(pepita, duroDeMatar,4,"Bruce willis es un pescao");
		
		//Asercion
		assertTrue(moovies.usuarioNoTieneCalificaciones(pepita));
	}
	
	@Test
	public void test03_Cuando_Un_Usuario_Intenta_Dar_Un_Rating_Por_Encima_De_Cinco_O_Menor_A_Uno_El_Sistema_No_Lo_Registra() {
		
		//setUp: Se guarda la reproduccion individual que recibe pepita cuando le indica a moovies que quieren ver una pelicula.
		Reproduccion reproduccionDeDuroDeMatarDePepita = moovies.verPelicula(duroDeMatar);
		
		//ejecucion:Le da play a la reproduccione obtenida e intenta calificarlas con ratings fuera del rango permitido.
		moovies.darlePlayAReproduccion(reproduccionDeDuroDeMatarDePepita, pepita);
		
		moovies.calificarPelicula(pepita, duroDeMatar,20,"Bruce willis es un pescao");
		moovies.calificarPelicula(pepita, duroDeMatar,-2,"Bruce willis es un pescao");
		
		//asercion
		assertTrue(moovies.usuarioNoTieneCalificaciones(pepita));
	}
	
}
